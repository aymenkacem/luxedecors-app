<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/*
 *	@author 	     :    Creativeitem
 *	date		     :    25 July, 2015
 *	Item             :    Ekattor School Management System ios Application
 *  Specification    :    Mobile app response, JSON formatted data for iOS & android app
 *	Portfolio        :    http://codecanyon.net/user/Creativeitem
 *  Website          :    http://www.creativeitem.com
 *	Support          :    http://support.creativeitem.com
 */
use Zend\Http\PhpEnvironment\Request;
use Firebase\JWT\JWT;

class Api extends Site_Controller
{
    var $secretKey= 'gPNDBu4cyD90s4mdhU9geybbMTPfyfyZNbd6OQdlvtscMwKMry7byu68fW79Y9EAqTpX/qgwyw37e9wovHixsQ==';
    var $algorithm= 'HS512';
    var $data=array();


	function __construct()
	{
    parent::Site_Controller();

    $this->rapyd->load("dataobject");

		//**************** DEFINE LANG *********************/
		if (@$this->rapyd->language->key_language)
			define('SITE_LANG',$this->rapyd->language->key_language);
		elseif(@$this->rapyd->session->get('language')){
			$key_lang=array_search($this->rapyd->session->get('language'),$this->config->item('langues_folder'));
			define('SITE_LANG',$key_lang);
		}else{
			define('SITE_LANG',DEFAULT_LANG);
			$this->rapyd->language->set_language('french');
		}
		$this->lang->load('frontend');
		$this->lang->load('table');
		$lien=SITE_LANG.'/';
		foreach($this->uri->segment_array() as $uri)
			$lien.=$uri.'/';
		define('LIEN',$lien);
		array_walk_recursive($_POST, 'clean_callback');

        define('LANG','fr');
        $this->load->model('api_model');
        $this->load->library('Paypal_Lib');
    		$this->lang->load('countries');
    		$this->lang->load('regions');
        $this->load->model('cart_model');

        if($client=$this->validate_auth_key()){
            $this->data['token'] = $this->jwt($client);
        }

        if (isset($client->customer_id) && $client->customer_id) {
          define('CUSTOMER_ID', $client->customer_id);
        } else {
          define('CUSTOMER_ID', false);
        }

        if(isset($client->type) && $client->type==2)
          define('REPRESENTATIVE',TRUE);
        else
          define('REPRESENTATIVE',FALSE);

        if(isset($client->company_id) && $client->company_id)
          define('COMPANY',$client->company_id);
        else
          define('COMPANY',false);
    }

    function home(){
        $this->data['banners']=$this->api_model->getBanner();
        $this->data['featuredProducts']=$this->api_model->getProductFeatured();

        header('Content-Type: application/json');
        echo json_encode($this->data);
    }

    function editProfile() {
      $this->rapyd->load('dataform','dataobject');
      $do = new DataObject("customer");
      $do->load(CUSTOMER_ID);
      $form=new DataForm(null,$do);

      $rules['first_name']='required|xss_clean';
      $rules['last_name']='required|xss_clean';
      $rules['email']='required|valid_email|unique|xss_clean';
      if ($this->input->post('passwordCheckBox') == 'True') {
        $rules['password']='required|min_length[4]|xss_clean';
  			$rules['conf_password']='required|matches[password]|xss_clean';
      }

      $form->validation->set_message(array('valid_email'=>lang('check_email'),'required'=>lang('required_error')));
			$form->validation->set_rules($rules);
			$form->validation->set_error_delimiters('', '');
      header('Content-Type: application/json');
			if($form->validation->run()) {
        $data = array(
          'email'      => $this->input->post('email'),
          'first_name' => $this->input->post('first_name'),
          'last_name'  => $this->input->post('last_name'),
          'newsletter' => $this->input->post('newsletter') ? $this->input->post('newsletter') : null,
          'tax_number' => $this->input->post('tax_number'),
          'password'   => $this->encrypt->encode($this->input->post('password')),
          'update_on'  => date("Y-m-d H:i:s"),
        );
        $this->db->where('customer_id', CUSTOMER_ID);
    		$this->db->update('customer', $data);

        $member = $this->api_model->getCustomerById(CUSTOMER_ID);
        if ($member->company_id) {
          $member->company_name = $this->getCompanyName($member->company_id);
        }

        echo json_encode(array('success'=> true, 'user' => $member)); exit();
			}

      echo json_encode(array('success'=> false, 'errors' => $this->validation->error_array_field)); exit();
    }

    function editAddress() {
      $this->rapyd->load('dataform','dataobject');
      $do = new DataObject("customer");
      $do->load(CUSTOMER_ID);
      $form=new DataForm(null,$do);

      $rules['address']='required|xss_clean';
      $rules['city']='required|xss_clean';
      $rules['postal_code']='required|xss_clean';
      $rules['phone']='required|numeric_phone|xss_clean';
      $rules['fax']='required|numeric_phone|xss_clean';
      $rules['state']='required|xss_clean';

      $form->validation->set_message(array('required'=>lang('required_error')));
			$form->validation->set_rules($rules);
			$form->validation->set_error_delimiters('', '');
      header('Content-Type: application/json');
			if($form->validation->run()) {
        $data = array(
          'company' => $this->input->post('company'),
          'address' => $this->input->post('address'),
          'city'  => $this->input->post('city'),
          'postal_code' => $this->input->post('postal_code'),
          'phone' => $this->input->post('phone'),
          'fax'   => $this->input->post('fax'),
          'country'   => 'TN',
          'state'   => $this->input->post('state'),
          'update_on'  => date("Y-m-d H:i:s"),
        );
        $this->db->where('customer_id', CUSTOMER_ID);
    		$this->db->update('customer', $data);

        $member = $this->api_model->getCustomerById(CUSTOMER_ID);
        if ($member->company_id) {
          $member->company_name = $this->getCompanyName($member->company_id);
        }

        echo json_encode(array('success'=> true, 'user' => $member)); exit();
			}

      echo json_encode(array('success'=> false, 'errors' => $this->validation->error_array_field)); exit();
    }

    function getInfo(){
        $this->load->model('checkout_model','checkout');
        $this->data['menu']=$this->api_model->getMenu();
        foreach ($this->data['menu'] as $key => $value) {
    			if (getPromotion($value['categorie_id'])) {
    					$this->data['menu'][$key]['submenu'][] = array('name'=> lang('On discount'), 'img' => base_url().'assets/img/category-promotion.png', 'promo' => true);
    			}
    		}
        if (getPromotion()) {
          $this->data['menu'][] = array('name'=> lang('On discount'), 'img' => base_url().'assets/img/category-promotion.png', 'promo' => true);
        }
        $this->data['sidebox']=$this->api_model->getSideBox();
        $this->data['footerMenu']=$this->api_model->getFooterMenu();
        $this->data['footerServices'] = array(lang("Free shipping around Tunisia"), lang("Online payment or at delivery"), lang("100% secure payment"));
        $this->data['year'] = date("Y");
        $this->data['shipping_fee'] = $this->checkout->getColissimo('TN',0);
        $this->data['free_transport'] = getTransport_free();
        $this->data['mobile'] = getMessage_mail(122);
        $this->data['tel'] = getMessage_mail(123);

        echo json_encode($this->data);
    }

    function billingList() {
      $this->load->model('customer_model','customer_model');
      $billing_list = $this->customer_model->getBilling();
      header('Content-Type: application/json');
      echo json_encode($billing_list);
    }

    function billingView() {
      $this->load->model('customer_model','customer_model');
      $billing_list = $this->customer_model->getBilling();

      $billing_id=$this->uri->segment(3);
  		$this->lang->load('countries');
  		$this->lang->load('regions');

  		$query=$this->db->getwhere('customer_address',array('billing_id'=>$billing_id));
  		$customer_address=$query->row();

  		$this->db->select('b.*,c.first_name,c.last_name,DATE_FORMAT(b.created_at,\''.lang("date_format_sql").'\') AS date_creation');
  		$this->db->from("billing AS b");
  		$this->db->join("customer AS c","b.customer_id=c.customer_id");
  		$this->db->where("b.billing_id",$billing_id);
  		if(COMPANY){
  			$this->db->where("b.customer_id IN (SELECT customer.customer_id FROM customer WHERE customer.company_id = ".COMPANY." )");
  		}else
  			$this->db->where("b.customer_id",CUSTOMER_ID);

  		$query0=$this->db->get();
  		$billing=$query0->row();
  		if($query0->num_rows()>0)
  		{
  			/*
  			*	Selection des news à afficher dans la datagrid
  			*/
  			$this->db->select('bi.*,bi.product_name as title');
        $this->db->select('(SELECT concat("'. base_url().$this->config->item('upload_product') .'", product.image) FROM (product) WHERE product.product_id = bi.product_id) AS image');
  			$this->db->from("billing_item AS bi");
  			$this->db->where("bi.billing_id",$billing_id);
  			$billing_item=$this->db->get();
  		} else
  		{
  			exit('redirect');
  		}
  		$data["customer_address"] =$customer_address;
  		$data["billing"] =$billing;
  		$data["billing_item"] =$billing_item->result();
      foreach ($data["billing_item"] as $key => $item) {
        if($item->option_list){
           $data["billing_item"][$key]->option_list = json_encode(unserialize($item->option_list));
           $data["billing_item"][$key]->attr_list = json_encode(unserialize($item->attr_list));
        }
      }

      header('Content-Type: application/json');
      echo json_encode($data);
    }

    function productList(){
        $this->data=array_merge($this->data,$this->api_model->getProductList());
        header('Content-Type: application/json');
        echo json_encode($this->data);
    }


    function productDetail(){
        $this->data=array_merge($this->data,$this->api_model->getProduct());
        header('Content-Type: application/json');
        echo json_encode($this->data);
    }

    public function search()
    {
      $this->rapyd->load_db();
      $ajax_request=$this->input->post('ajax_request');
      $page = $this->input->post('page');
      if (!$page) {
        $page = 0;
      }
      $this->rapyd->load('datafilter');

      $filter = new DataFilter("");

      $filter->db->select('p.*,pd.*,p.promotion,TO_DAYS(p.price_special_from) AS special_from,TO_DAYS(p.price_special_to) AS special_to');
      //if($ajax_request) $filter->db->select('COUNT(*) AS totalrows');
      $filter->db->from("product AS p");
      $filter->db->join("product_detail AS pd","p.product_id=pd.product_id");
      $filter->db->where("pd.lang",SITE_LANG);
      $filter->db->where("p.active",1);
      $filter->db->where("(SELECT COUNT(pc.categorie_id) FROM (product_categorie as pc) where pc.product_id=p.product_id) >",0);
      $filter->db->orderby ("p.created_at",'DESC');

      $filter->title = new inputField("title", "title");
      $filter->buttons("reset","search");
      if($filter->_action=='search')
      {
         $filter->_action='search_all';
         $filter->post_field='product_ref';
         $filter->search_fields=array('p.product_ref','pd.title','pd.description','pd.keyword_ref');
      }

      $filter->build();
      $query =$filter->db->get();
      $data['total_products'] = $query->num_rows();
      $data['next_page'] = ((($page + 1) * 24) < $data['total_products']) ? $page + 1 : false;
      if ($ajax_request) {
        $data['product_list'] = array_slice($query->result(), 0, 5);
      } else {
        $data['product_list'] = array_slice($query->result(), $page * 24, 24);
      }

      foreach ($data['product_list'] as $key => $value) {
        $data['product_list'][$key]->image = base_url().$this->config->item('upload_product') . thumb_name($data['product_list'][$key]->image, ($ajax_request) ? '_thumb2' :'_thumb1');
      }

      header('Content-Type: application/json');
      echo json_encode($data);
    }

    public function productPromotion() {
        $categorie_id= $this->input->post('categorie_id');
        $this->rapyd->load_db();
        $this->rapyd->load('datafilter');
        $filter = new DataFilter("");
        $date =to_days(date("Y-m-d"));
        $_POST['product_ref']='En promotion';

        $page = $this->input->post('page');
        if (!$page) {
          $page = 0;
        }

        if(is_numeric($this->uri->segment(6)) || ( $this->uri->segment(7)&& is_numeric($this->uri->segment(7)) ) ){
          $_POST['ajaxType']='ajax';
        }else{
          unset($_POST['ajaxType']);
        }

        $filter->db->select('p.*,pd.*,p.promotion,TO_DAYS(p.price_special_from) AS special_from,TO_DAYS(p.price_special_to) AS special_to');
        //if($ajax_request) $filter->db->select('COUNT(*) AS totalrows');
        $filter->db->from("product AS p");
        $filter->db->join("product_detail AS pd","p.product_id=pd.product_id");
        $filter->db->where("pd.lang",SITE_LANG);
        $filter->db->where("p.active",1);

        if($categorie_id){
            $child=$this->product_model->categorie_child($categorie_id);
            $where_categorie=implode(',',$child);
            if($where_categorie)
                $filter->db->where("(SELECT COUNT(pc.categorie_id) FROM (product_categorie as pc) where pc.product_id=p.product_id AND pc.categorie_id IN (".$where_categorie.")) >",0);
        }else
            $filter->db->where("(SELECT COUNT(pc.categorie_id) FROM (product_categorie as pc) where pc.product_id=p.product_id) >",0);

        $filter->db->where("p.promotion >",0);
        $filter->db->where("TO_DAYS(p.price_special_from) <=",$date);
        $filter->db->where("TO_DAYS(p.price_special_to) >=",$date);
        $filter->db->orderby ("p.created_at",'DESC');

        // $filter->build();

        $query =$filter->db->get();
        $data['total_products'] = $query->num_rows();
        $data['next_page'] = ((($page + 1) * 24) < $data['total_products']) ? $page + 1 : false;
        $data['product_list'] = array_slice($query->result(), $page * 24, 24);

        foreach ($data['product_list'] as $key => $value) {
          $data['product_list'][$key]->image = base_url().$this->config->item('upload_product') . thumb_name($data['product_list'][$key]->image, '_thumb1');
        }

        header('Content-Type: application/json');
        echo json_encode($data);

    }

    public function dashboard() {
      $this->load->model('customer_model');
      $data['totalOrders']= $this->customer_model->getBilling(FALSE,TRUE);
      $data['dateLastOrder']=($this->customer_model->getBilling(true)) ? $this->customer_model->getBilling(true)->date_creation: FALSE;
      header('Content-Type: application/json');
      echo json_encode($data);
    }

    public function newsletter()
  	{
  		$this->load->library('validation');
  		$rules['email'] = "required|valid_email";
  		if(!isset($_SESSION['newsletter_in_session'])) $_SESSION['newsletter_in_session']=0;
  		$this->validation->set_rules($rules);

  		$response=false;
  		$message='';

  		if ($this->validation->run() == FALSE)
  		{
  			$message= lang("check_email");
  		}
  		elseif($_SESSION['newsletter_in_session']>4)
  		{
  			$message= lang("try_again_later");
  		}
  		else
  		{
  			$email=$this->input->post('email');
  			$query=$this->db->getwhere('newsletter',array('email'=>$email));
  			if($query->num_rows())
  			{
  				$message= lang("already_subscribed");
  			}
  			else
  			{
  				$data = array(
  					   'email' => $email ,
  					   'site_id' => 1
  					);
  				$this->db->insert('newsletter', $data);
  				$_SESSION['newsletter_in_session']++;
  				$message= lang("inscription");
  				$response=true;
  			}
  		}

      header('Content-Type: application/json');
  		echo json_encode(array('valid'=>$response,'message'=>$message));
  	}

    public function validateCoupon() {
      $this->load->library('validation');
      $rules['coupon_code'] = "required";
      $this->validation->set_rules($rules);
      if ($this->validation->run() == FALSE)
  		{
        exit('coupon_code required');
  		}

      $coupon_code = $this->input->post('coupon_code');
      $coupon=$this->db->select('c.*,cd.*')->from("project_card_discount_code AS cd")->join("project_card_discount AS c","c.card_id=cd.card_id")->where(array("card_discount_code"=>$coupon_code))->get()->row();
      if($coupon){
        if($coupon->code_used || strtotime($coupon->end_date) < strtotime(date('Y-m-d'))){
          $data = ['success' => false, 'error' => lang('coupon_error')];
        } else
          $data = ['success' => true, 'coupon' => $coupon];

      }else{
        $data = ['success' => false, 'error' => lang('coupon_error')];
      }
      header('Content-Type: application/json');
      echo json_encode($data);
  	}

    public function cartItems() {
      $jsonArray = file_get_contents('php://input');
  		$cart =json_decode($jsonArray,true);

      if( defined('REPRESENTATIVE') && REPRESENTATIVE) {
        $promotions = $this->api_model->getRepresentativePromotion($cart);
      }

		  $cart_items = $this->api_model->getCartItems($cart);

      $date=to_days(date("Y-m-d"));
      foreach($cart_items as $key => $item) {
        $cart_items[$key]->image = base_url().$this->config->item('upload_product') . thumb_name($cart_items[$key]->image, '_thumb2');
        $cart_items[$key]->quantity = $cart[$key]['quantity'];
        $promotion=FALSE;
        $cart_items[$key]->promotion_value = 0;
        // if($item->laminage) $price+=$item->laminage;
        // if($item->encadrement) $price+=$item->encadrement;

        if($item->promotion && $date >= $item->special_from && $date <= $item->special_to ) {
            $promotion=TRUE;
            $cart_items[$key]->promo_price = ($item->price*(100-$item->promotion)/100);
            $cart_items[$key]->promotion_value = $item->promotion;
        }
        if(isset($promotions[$item->product_id]) && $promotions[$item->product_id] && !($promotion && $item->promotion > $promotions[$item->product_id]))
        {
            $cart_items[$key]->promo_price =($item->price*(100-$promotions[$item->product_id])/100);
            $cart_items[$key]->promotion_value =$promotions[$item->product_id];
        }
      }

      header('Content-Type: application/json');
      echo json_encode($cart_items);
    }

    public function processCheckout()
    {
      $this->rapyd->load('dataform','dataobject');
      $do = new DataObject("customer_address");
      $form=new DataForm(null,$do);

      $rules['first_name']='required|xss_clean';
      $rules['last_name']='required|xss_clean';
      $rules['address']='required|xss_clean';
      $rules['city']='required|xss_clean';
      $rules['postal_code']='required|xss_clean';
      $rules['phone']='required|numeric_phone|xss_clean';
      $rules['state']='required|xss_clean';

      if(json_decode($this->input->post('shipping_different')))
  		{
  			$rules['sh_first_name']='required|xss_clean';
  			$rules['sh_last_name']='required|xss_clean';
  			$rules['sh_address']='required|xss_clean';
  			$rules['sh_city']='required|xss_clean';
        $rules['sh_postal_code']='required|xss_clean';
        $rules['sh_phone']='required|numeric_phone|xss_clean';
        $rules['sh_state']='required|xss_clean';
  		}

      $form->validation->set_message(array('valid_email'=>lang('check_email'),'required'=>lang('required_error')));
			$form->validation->set_rules($rules);
			$form->validation->set_error_delimiters('', '');

      header('Content-Type: application/json');
			if($form->validation->run()) {
        echo json_encode(array('success'=> true)); exit();
			}

      echo json_encode(array('success'=> false, 'errors' => $this->validation->error_array_field)); exit();
    }

    public function confirmCheckout() {
      $this->load->model('checkout_model','checkout');

      $jsonArray = file_get_contents('php://input');
  		$jsonArray =json_decode($jsonArray,true);
  		$cart_items = $jsonArray['cart'];
  		$methode=$jsonArray['paymentMethod'];
      $customerAddress = $jsonArray['customerAddress'];
      if (isset($jsonArray['coupon_code'])) {
        $_SESSION['coupon_code'] = $jsonArray['coupon_code'];
      }

  		$cart= $this->api_model->setCart($cart_items);
      $this->api_model->setCustomerAddress($customerAddress, $cart);

  		$status=($methode=='delivery')?3:1;
  		$billing_id=$this->checkout->new_billing($cart,$status);

      if($methode=='delivery'){
  			if($billing_id){
  				$customer = $this->db->getwhere('customer', array('customer_id' => CUSTOMER_ID))->row();
  				$this->checkout->mail_delivery($customer,$billing_id);
  			}
  		}
  		echo json_encode(['billing_id' => $billing_id]);
    }

    public function gpgCheckout()
    {
      $billing_id=$this->uri->segment(3);
      $query = $this->db->getwhere('billing', array('billing_id' => $billing_id));
      $billing=$query->row();
      $do = new DataObject("customer");
      $do->load($billing->customer_id);
      $this->load->library('session');
  		$session_id = $this->session->userdata('session_id');

      $items=$this->db->getwhere('billing_item', array('billing_id' => $billing_id))->result();
      $orderProducts=array();
      foreach($items as $item){
        $orderProducts[]=$item->product_name.' ( sku: '.$item->sku.' / Quantite: '.$item->quantity.' )';
      }

      $country=lang('country');
      $regions=lang('regions');
      $customer_address = $this->db->getwhere('customer_address', array('billing_id' => $billing_id))->row();

      $signature=sha1('MAR287xc@exQ90'.$billing->billing_id.number_format($billing->total_price, 3, '', '').'TND');

      $state=$regions[$customer_address->country][$customer_address->state]['name'];
      $adress=$customer_address->address.', '.$customer_address->city.', '.$customer_address->postal_code.', '.$state;

      $this->paypal_lib->add_field('NumSite', 'MAR287');
      $this->paypal_lib->add_field('Password', md5('xc@exQ90'));
      $this->paypal_lib->add_field('Currency', 'TND');
      $this->paypal_lib->add_field('Language', 'fr');
      $this->paypal_lib->add_field('orderID', $billing->billing_id);
      $this->paypal_lib->add_field('Amount', number_format($billing->total_price, 3, '', ''));
      $this->paypal_lib->add_field('EMAIL', $do->get('email'));
      $this->paypal_lib->add_field('CustLastName', $do->get('last_name'));
      $this->paypal_lib->add_field('CustFirstName', $do->get('first_name'));
      $this->paypal_lib->add_field('CustAddress',$adress );
      $this->paypal_lib->add_field('CustZIP',$customer_address->postal_code);
      $this->paypal_lib->add_field('CustCity',$customer_address->city );
      $this->paypal_lib->add_field('CustTel',$customer_address->phone );
      $this->paypal_lib->add_field('PayementType',1 );
      $this->paypal_lib->add_field('MerchandSession', $session_id );
      $this->paypal_lib->add_field('orderProducts','Produit LUXE DECORS : '.implode($orderProducts,' + ') );
      $this->paypal_lib->add_field('signature', $signature);

      $this->paypal_lib->paypal_auto_form();
    }

    public function signup() {
      $this->rapyd->load('dataform','dataobject');
      $do = new DataObject("customer");
      $form=new DataForm(null,$do);

      $rules['first_name']='required|xss_clean';
      $rules['last_name']='required|xss_clean';
      $rules['email']='required|valid_email|unique|xss_clean';
			$rules['password']='required|min_length[4]|xss_clean';
			$rules['conf_password']='required|matches[password]|xss_clean';

      $form->validation->set_message(array('valid_email'=>lang('check_email'),'required'=>lang('required_error')));
			$form->validation->set_rules($rules);
			$form->validation->set_error_delimiters('', '');
      header('Content-Type: application/json');
			if($form->validation->run()) {
        $data = array(
          'email'     => $this->input->post('email'),
          'first_name' => $this->input->post('first_name'),
          'last_name'  => $this->input->post('last_name'),
          'newsletter' => $this->input->post('newsletter') ? $this->input->post('newsletter') : null,
          'password' => $this->encrypt->encode($this->input->post('password')),
          'created_at'  => date("Y-m-d H:i:s"),
          'update_on'  => date("Y-m-d H:i:s"),
          'customer_lang'    => SITE_LANG
        );
    		$this->db->insert('customer',$data);
				$this->mail_welcome(array('email'=>$data['email'],'first_name'=>$data['first_name'],'last_name'=>$data['last_name']));

        echo json_encode(array('success'=> true)); exit();
			}

      echo json_encode(array('success'=> false, 'errors' => $this->validation->error_array_field)); exit();
    }

    function mail_welcome($customer)
  	{
  		$this->load->library('email');
  		$mail_config=$this->config->item('mail_config');
  		$this->email->initialize($mail_config);
  		$from_mail=getContact_mail();
  		$to_mail=$customer['email'];
  		$data=$customer;
  		$data['message']=getMessage_mail(360);

  		$view='welcome_mail';
  		$subject=lang('Welcome to').' '.lang('our_site');

  		$msg=$this->load->view('public/payment/customer/'.$view,$data,TRUE);
  		$this->email->from($this->config->item('smtp_user'), $from_mail);
  		$this->email->to($to_mail);
  		$this->email->subject($subject);
  		$this->email->message($msg);
  		if(!$this->email->send()){
  			echo 'Error while sending the mail';
  			exit();
  		}
  		return true;
  	}

    public function contact() {
      $valid = true;
      header('Content-Type: application/json');
      if ($valid) {
        echo json_encode(array('success'=> true));
      } else {
        echo json_encode(array('success'=> false, 'errors' => array('email'=> 'Votre adresse email est invalide')));
      }
    }

    public function retrievePass()
    {
      header('Content-Type: application/json');
      if ($valid) {
        echo json_encode(array('success'=> true));
      } else {
        echo json_encode(array('success'=> false, 'errors' => array('email'=> 'Votre adresse email est invalide')));
      }
    }


    function auth(){
      $this->load->model('customer_model');
      $jsonArray = file_get_contents('php://input');
      $jsonArray =json_decode($jsonArray,true);

      if (isset($jsonArray['facebookLogin'])) {
        $this->config->load("facebook", TRUE);
    		$config = $this->config->item('facebook');
    		$this->load->library('Facebook', $config);

        $this->facebook->setAccessToken($jsonArray['accessToken']);
    		$user = $this->facebook->api('/me?fields=first_name,last_name,email');

  			$ismember = $this->customer_model->isMember($user['email']);
  			if($ismember == null){
  				$this->customer_model->createFBUser($user);
  				$this->mail_welcome($user);
  				$r = $this->customer_model->isMember($user['email']);	// Sign up User
  			} else {
          $r=$ismember;
        }
        if ($r->company_id) {
          $r->company_name = $this->getCompanyName($r->company_id);
        }
        echo json_encode(['token' => $this->jwt($r), 'user' => $r]);
      } else if (isset($jsonArray['googleSignin'])) {
        /*$this->load->library('Gplus');

    		$client = new apiClient();
    		$client->setApplicationName("Luxe Decors");
        $client->setClientId("729739307904-fe9fsjhukcui2tm6e4ubqdq4hr74vl5h.apps.googleusercontent.com");
        $client->setClientSecret("bm84kZBgsJUZMT1b8NaS9cc6");
        $client->setAccessType('offline');

    		$client->setScopes(array('https://www.googleapis.com/auth/plus.me'));
        $_GET['code'] = $jsonArray['accessToken'];
        var_dump($_GET['code']);
        $client->authenticate();
        // exit();
        //$accessToken = json_encode(array('access_token' => $jsonArray['accessToken']));
        //$client->setAccessToken($accessToken);

        $plus = new apiPlusService($client);
        //$oauth2Service = new apiOauth2Service($client);

  			$me = $plus->people->get('me');
  			//$userinfo = $oauth2Service->userinfo->get();
  			$user = array(
  				//'email' => $userinfo['email'],
  				'first_name' => $me['name']['givenName'],
  				'last_name' => $me['name']['familyName'],
  				);
          */


        $name = explode(" ", $jsonArray['accessToken']['displayName'], 2);
        $user = array(
  				'email' => $jsonArray['accessToken']['email'],
  				'first_name' => $name[0],
  				'last_name' => $name[1],
				);
  			$ismember = $this->customer_model->isMember($user['email']);
  			if($ismember == null){
  				$this->customer_model->createGUser($user); 	// Sign up User
  				$this->mail_welcome($user);
  				$r = $this->customer_model-> isMember($user['email']);	// Sign up User
  			}else {
          $r=$ismember;
        }
        if ($r->company_id) {
          $r->company_name = $this->getCompanyName($r->company_id);
        }
        echo json_encode(['token' => $this->jwt($r), 'user' => $r]);
      } else {
        $email = $jsonArray['email'];
        $password = $jsonArray['password'];

        $this->load->model('user_model','user');
        if($r = $this->user->try_login($email,$password)){
          if ($r->company_id) {
            $r->company_name = $this->getCompanyName($r->company_id);
          }
          echo json_encode(['token' => $this->jwt($r), 'user' => $r]);
        }else{
            header('HTTP/1.0 401 Unauthorized');
            echo json_encode(['error' => 'invalid_credentials', 'email' => 'Vérifier votre courriel', 'password' => 'Vérifier votre mot de passe']);
        }
      }
    }

    function getCompanyName ($company_id) {
				$comp = new DataObject("company");
				$comp->load($company_id);
        return $comp->get('name');
    }

    function jwt($client){
        $tokenId    = base64_encode(mcrypt_create_iv(32));
        $issuedAt   = time();
        $notBefore  = $issuedAt;
        $expire     = $notBefore + 2592000; // Adding 60 seconds
        $serverName = base_url();

        /*
         * Create the token as an array
         */
        $data = [
            'iat'  => $issuedAt,         // Issued at: time when the token was generated
            'jti'  => $tokenId,          // Json Token Id: an unique identifier for the token
            'iss'  => $serverName,       // Issuer
            'nbf'  => $notBefore,        // Not before
            'exp'  => $expire,           // Expire
            'data' => [                  // Data related to the signer user
                'customer_id'   => $client->customer_id, // userid from the users table
                'first_name' => $client->first_name, // User name
                'last_name' => $client->last_name, // User name
                'type' => $client->type,
                'company_id' => $client->company_id
            ]
        ];
        $jwt = JWT::encode(
            $data,      //Data to be encoded in the JWT
            $this->secretKey, // The signing key
            $this->algorithm  // Algorithm used to sign the token, see https://tools.ietf.org/html/draft-ietf-jose-json-web-algorithms-40#section-3
            );

        return $jwt;
    }

    // authentication_key validation
    function validate_auth_key() {
        $request = new Request();
        $authHeader = $request->getHeader('Auth');
        if ($authHeader) {
            /*
             * Extract the jwt from the Bearer
             */
            list($jwt) = sscanf( $authHeader->toString(), 'Auth: Bearer %s');

            if ($jwt) {
                try {
                    $token = JWT::decode($jwt, $this->secretKey, [$this->algorithm]);
                    return (object)$token->data;
                } catch (Exception $e) {echo 'Message: ' .$e->getMessage();}
            }
        }
        return FALSE;
    }

}
