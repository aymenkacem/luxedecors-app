$(document).ready(function()
{
	$('#cart-details .remove button').click(function(e) {
		if ($(this).children().hasClass('loading')) return;
		$(this).children().addClass('loading icon-spin');
		var itemLine=$(this).closest('tr');
		var cart_item_id=itemLine.data('id');
		console.log(cart_item_id);
		var lien=base_url+site_lang+'/payment/cart/delete_ajax/'+cart_item_id;

		$.ajax({
			   type: "POST",
			   url: ''+lien,
			   dataType: "json",
			   success: function(response){
			   		itemLine.fadeOut(500, function () {
			   			$.cartWidgetRefresh(response.nbre);
			   			if ( $( "#coupon_div" ).length && !response.promotion) {
						    $( "#coupon_div" ).hide();
						}
						if(response.nbre)
							$('#subtotal').html(response.total);
						if(!itemLine.siblings(':visible').length) {
							$('#cart').hide();
							$('#no-item').fadeIn();
						}
			   		} );
			   }
		});
	});

	var xhr=$.ajax();
	var timeoutId=0;
	$('.input-quantity').on('input propertychange',function(){
		var new_value=parseInt($(this).val());
		var value=$(this).siblings('.qty_value_input').val();
		var itemLine=$(this).closest('tr');
		var element=$(this);
		if($.isNumeric(new_value) && new_value!=value){
			clearTimeout(timeoutId);
			timeoutId = setTimeout(function(){ $.cartUpdateItem(itemLine,new_value,element) }, 650);
		}
	});
	$('#cart-details button.plus').click(function(e) {
		var new_value=parseInt($(this).siblings('.input-quantity').val())+1;
		$(this).siblings('.input-quantity').val(new_value);
		var value=$(this).siblings('.qty_value_input').val();
		var itemLine=$(this).closest('tr');
		var element=$(this).siblings('.input-quantity');
		if(new_value!=value){
			clearTimeout(timeoutId);
			timeoutId = setTimeout(function(){ $.cartUpdateItem(itemLine,new_value,element) }, 650);			
		}
	});

	$('#cart-details button.minus').click(function(e) {
		var new_value=parseInt($(this).siblings('.input-quantity').val())-1;
		if(!new_value) return;
		$(this).siblings('.input-quantity').val(new_value);
		var value=$(this).siblings('.qty_value_input').val();
		var itemLine=$(this).closest('tr');
		var element=$(this).siblings('.input-quantity');
		if(new_value!=value){
			clearTimeout(timeoutId);
			timeoutId = setTimeout(function(){ $.cartUpdateItem(itemLine,new_value,element) }, 650);			
		}
	});

	$.cartUpdateItem = function (itemLine,qty,element) {
		var cart_item_id=itemLine.data('id');
		var price=itemLine.find('.price');
		var price_=itemLine.find('.price_').val();
		price.empty().append('<span class="loading icon-spin"></span>');
		var lien=base_url+'/payment/cart/update_item/'+cart_item_id;
		
		xhr.abort();
		xhr=$.ajax({
		   type: "POST",
		   url: ''+lien,
		   data: ({quantity : qty,price : price_}),
		   dataType: "json",
		   success: function(response){
				$('#subtotal').html(response.total);
				$.cartWidgetRefresh(response.nbre);
				itemLine.find('.qty_value_input').val(qty);
				price.empty().html(response.price_item);
				if ( $( "#coupon_div" ).length) {
				    $( "#coupon_div" ).toggle(response.promotion);
				}
				if ("message" in response) {
				   	alertify.error(response.message);
				   	element.val(response.qty);
				}
				if(parseFloat(response.total)) $( ".main-footer .button" ).show(); else $( ".main-footer .button" ).hide();
				console.log(parseFloat(response.total));
			}
		});
    }

});