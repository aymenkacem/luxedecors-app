$(document).ready(function()
{
	// var stores= [
	// 	{"latlng": [45.549283,-73.188579], "address":"510 Montée des Trentes,<br>Mont St-Hilaire, QC J3H 2R8", "name":"La Maison Amérindienne", "phone":"Tel: (450) 464-2500" },
	// 	{"latlng": [45.539275,-73.509366], "address":"123 St-Charles Ouest,<br>Longueuil, QC J4H 1G7", "name":"William J. Walter", "phone":"Tel: (450) 748-0744" },
	// 	{"latlng": [45.502036,-73.512777], "address":"436 av Victoria,<br>ST-Lambert, QC J4P 2J4", "name":"Terre Essence", "phone":"T. (450) 812-0222" },
	// 	{"latlng": [45.529608,-73.344584], "address":"1380 de Montarville,<br>ST-Bruno, QC J3V 3T5", "name":"William J. Walter", "phone":"Tel: (450) 461-3033" },
	// 	{"latlng": [58.105819,-68.404655], "address":"5205 Airport Road<br> Kuujjuaq, Nunavik, Qc, Canada, J0M 1C0", "name":"TIVI Galleries", "phone":"Tel: 1 (800) 964-2465" },
	// 	{"latlng": [45.536275,-73.613872], "address":"280 Place du Marché du Nord<br> Montréal, QC H2S 1A1", "name":"Marché Des Saveurs Du Québec", "phone":"Tel: (514) 271-3811" },
	// 	{"latlng": [45.477875,-73.60139], "address":"330A av. Victoria<br> Westmount QC H3Z 2M8", "name":"L'Autre choix", "phone":"Tel: (514) 369-1888" },
	// 	{"latlng": [45.336163,-73.267983], "address":"1005 Boul. Du Séminaire Nord<br> St-Jean sur Richelieu,  QC  J3A 1R7", "name":"Patisserie Bissonnette", "phone":"Tel: (514) 349-2375" },
	// 	{"latlng": [45.624279,-72.94635], "address":"1660, des Cascades<br>Saint-Hyacinthe, QC J2S 3H8", "name":"Les Passions de Manon", "phone":"Tel: (450) 250-6423" },
	// 	{"latlng": [45.483424,-72.759532], "address":"5, route 137<br>Ste-Cécile de Milton, QC J0E 2C0", "name":"Verger de la Colline", "phone":"Tel: (450) 777-2442" },
	// 	{"latlng": [58.100782,-68.411098], "address":"Immeuble 601, route de l'Aéroport, C.P. 729,<br>Kuujjuaq (Québec), J0M 1C0", "name":"Nunavik Créations", "phone":"Tel: (819) 964-1848" },
	// 	{"latlng": [61.358005,-117.659929], "address":"#1 Mackenzie Drive, Fort Providence<br> Northwest Territories, Canada, X0E-0L0", "name":"Snowshoe Inn (NWT) Ltd", "phone":"Tel: (867) 699-3511" },
	// 	{"latlng": [62.462109,-114.353083], "address":"5005 Bryson Drive<br> Yellowknife, Nt X1A 2A3", "name":"Gallery of the Midnight Sun", "phone":"Tel: (867) 873-8064" },
	// 	{"latlng": [35.8567859,10.597317], "address":"Immeuble Yasmine Laartiri, Avenue de la république<br>Hammam Sousse, 4011", "name":"Accès Leader", "phone":"Tel: (+216) 73 370 263" }
	// ];

	/*______________________________________________________________*/			
	/*_______________________GEOLOCALISATION_______________________*/
	/*____________________________________________________________*/
	
	/**************************************************************/
	/***************** CONFIGURATION GOOGLE MAP ******************/
	/************************************************************/
	//var site_name = "Company Name"; //Déplacé vers le contrôleur, utilisation de la variable de langue "our_site".
	var latlng = new google.maps.LatLng(35.4555555, 9.859999); //Zone d'affichage par defaut
	var zoom = 7;
	var maxDistance = 100;
	var defaultStoresList = $('#localisation_result').html();
	
	var map;
	var markersArray = [];
	var infobox;
	var bonhomme;
	var initial = true;
	
	var bounds = new google.maps.LatLngBounds();
	var geocoder = new google.maps.Geocoder();
	var userAddress ;
	var directionsDisplay = new google.maps.DirectionsRenderer({suppressMarkers:true, polylineOptions:{strokeColor: '#48812e'}});
	var directionsService = new google.maps.DirectionsService();
	 
	var markers = new Array ();
	for (var i=1; i<=13; i++) {
		markers[i] = new google.maps.MarkerImage(base_url+'assets/img/pins.png', new google.maps.Size(32, 47),new google.maps.Point(0,47*(i-1)), new google.maps.Point(16, 47) );
	}
	
	//var marker = new google.maps.MarkerImage(base_url+'assets/img/pin.png', new google.maps.Size(32, 47),new google.maps.Point(0,0), new google.maps.Point(16, 47) );
				
	var shadow = new google.maps.MarkerImage(base_url+'assets/img/shadow.png', new google.maps.Size(40, 24),new google.maps.Point(0,0), new google.maps.Point(9, 21));
	var pin_user = new google.maps.MarkerImage(base_url+'assets/img/pin_user.png', new google.maps.Size(27, 31),new google.maps.Point(0,0),new google.maps.Point(14, 25) );
	var pin_user_shadow = new google.maps.MarkerImage(base_url+'assets/img/pin_user.png', new google.maps.Size(43, 20),new google.maps.Point(27,0),new google.maps.Point(16, 13) );
	
	/*******************************************************/
	/************* GOOGLE MAPS API FUNCTIONS **************/
	/*****************************************************/
	//	initializeMap()			resetMap()		deleteOverlays()		
	//	showError()				addMarker()		displayDirection()		
	//	sortingDistances()		build()			
	//	formSubmit				locationClick
	
	function initializeMap () {
		if (!initial) return;
		
		/*var stylez = [ { featureType: "water", stylers: [ { color: "#dcd9c5" } ] },{ featureType: "landscape", stylers: [ { color: "#f7f7f2" } ] },{ featureType: "poi", stylers: [ { color: "#ebe9df" } ] },{ featureType: "road", stylers: [ { color: "#c3be9c" } ] },{ featureType: "road", elementType: "labels.text.fill", stylers: [ { color: "#ffffff" } ] },{ elementType: "labels.text.fill", stylers: [ { color: "#655f3d" } ] } ];*/
		var stylez = [{"featureType":"administrative","elementType":"all","stylers":[{"visibility":"on"},{"saturation":-100},{"lightness":20}]},{"featureType":"road","elementType":"all","stylers":[{"visibility":"on"},{"saturation":-100},{"lightness":40}]},{"featureType":"water","elementType":"all","stylers":[{"visibility":"on"},{"saturation":-10},{"lightness":30}]},{"featureType":"landscape.man_made","elementType":"all","stylers":[{"visibility":"simplified"},{"saturation":-60},{"lightness":10}]},{"featureType":"landscape.natural","elementType":"all","stylers":[{"visibility":"simplified"},{"saturation":-60},{"lightness":60}]},{"featureType":"poi","elementType":"all","stylers":[{"visibility":"off"},{"saturation":-100},{"lightness":60}]},{"featureType":"transit","elementType":"all","stylers":[{"visibility":"off"},{"saturation":-100},{"lightness":60}]}];

		var myOptions = {
			zoom: zoom,
			center: latlng,
			// panControl: true,
			streetViewControl: false,
			mapTypeControl : false,
			mapTypeId: google.maps.MapTypeId.ROADMAP,
			mapTypeControlOptions: {
				mapTypeIds: [google.maps.MapTypeId.ROADMAP, site_name]
			}
		};
		map = new google.maps.Map(document.getElementById("map"),myOptions);
		var jayzMapType = new google.maps.StyledMapType(stylez, { name: site_name});
		map.mapTypes.set(site_name, jayzMapType);
		map.setMapTypeId(site_name);
		
		for (i in stores) {
			stores[i].latlng = new google.maps.LatLng (stores[i].latlng[0],stores[i].latlng[1]);
			//addMarker(i, markers[i]);
		}
	
		initial = false;
	}
	
	function resetMap () {
		deleteOverlays();
		// for (i in stores) 
		// 	addMarker(i, markers[i]);

		map.setZoom(zoom);
		map.setCenter(latlng);
		$('#localisation_result').html(defaultStoresList);
	}
	
	function deleteOverlays() {
		if (markersArray) {
			for (i in markersArray) {
				markersArray[i].setMap(null);
			}
			markersArray.length = 0;
		}
		
		if (bonhomme) bonhomme.setMap(null);
		if (infobox) infobox.close();
		directionsDisplay.setMap(null);
		bounds = new google.maps.LatLngBounds();
	}
	
	function showError (msg) {
		$('#localisation_result').hide().empty().append('<div id="error">'+msg+'</div>').slideDown();
		deleteOverlays();
	}
	
	function addMarker(index, icon) {
		var marker = new google.maps.Marker({
			map: map,
			position: stores[index].latlng,
			icon: icon,
			shadow:shadow
		});
		google.maps.event.addListener(marker, "click", function() {
			var content = $('<div></div>');
			content.append('<h2>'+stores[index].name+'</h2><p>'+stores[index].address+'<br>'+stores[index].phone+'</p><span></span>');
			var myOptions = {
				content: content.get(0),
				alignBottom: true,
				pixelOffset: new google.maps.Size(-121, -62),
				closeBoxMargin: "-2px -4px 7px 7px",
				closeBoxURL: base_url+"assets/img/close_infobox.png",
				infoBoxClearance: new google.maps.Size(85, 20)
			};
					
			if (infobox) infobox.close();
			infobox = new InfoBox(myOptions);
			infobox.open(map, marker);
		});
		
		markersArray.push(marker);
	}
	
	function displayDirection (start, end) {
		var request = {
			origin:start,
			destination:end,
			travelMode: google.maps.TravelMode.DRIVING
		};
		directionsService.route(request, function(result, status) {
			if (status == google.maps.DirectionsStatus.OK) {
				directionsDisplay.setMap(map);
				directionsDisplay.setDirections(result);
				directionsDisplay.setOptions({polylineOptions:{strokeColor:'#ec008c'}});
			}
		});
		
		bonhomme = new google.maps.Marker({
			map:map,
			draggable:true,
			animation: google.maps.Animation.DROP,
			position: start,
			icon: pin_user,
			shadow: pin_user_shadow
		});
		google.maps.event.addListener(bonhomme, 'click', function () {
			 if (bonhomme.getAnimation() != null) {
				bonhomme.setAnimation(null);
			  } else {
				bonhomme.setAnimation(google.maps.Animation.BOUNCE);
			  }	
		});
		google.maps.event.addListener(bonhomme, 'dragend', function () {
			var pos = bonhomme.getPosition();
			var request = {
				origin:pos,
				destination:end,
				travelMode: google.maps.TravelMode.DRIVING
			};
			directionsService.route(request, function(result, status) {
				if (status == google.maps.DirectionsStatus.OK) {
				  directionsDisplay.setDirections(result);
				}
			});
		});
	}
	
	function sortingDistances(r){
		var t = new Array();
		for( var i= 0 ; i< r.length; i++){
			r[i].index = i;
		}
		for( var i= 0 ; i< r.length; i++){
			var pmax = -1; var vmax = -1;
			for ( var j=i; j< r.length; j++){
			   if( r[j].status==="OK" && r[j].distance.value > vmax) {
				   pmax = j;
				   vmax = r[j].distance.value;
			   }
			}
			if ( pmax >-1) {
				t.push(r[pmax]);
				var aux = r[i]; r[i] = r[pmax]; r[pmax] = aux;
			}
		}
		return t.reverse();
	}
	
	
	
	function build (response, status) {
		if (status == google.maps.DistanceMatrixStatus.OK) {
			$('#localisation_result').empty();
			deleteOverlays();
			var results = sortingDistances (response.rows[0].elements);
			if (!results.length) { 
				showError(noResultsError); 
				return;
			}
			displayDirection (userAddress, stores[results[0].index].latlng);
			var k=results.length<3?results.length:3;
			for (var j=0; j < k; j++) {
				addMarker(results[j].index, markers[j+1]);
				var innerhtml='<div class="location" addressIndex="'+results[j].index+'"><span class="marker marker'+(j+1)+'"><span>'+results[j].distance.text+
					'</span></span><div class="store_info"><h2>'+stores[results[j].index].name+'</h2><p>'+stores[results[j].index].address+
					'</p>'+stores[results[j].index].phone+'</div></div>';
	
				$('#localisation_result').hide().append (innerhtml).slideDown();
			}
		} else {
			showError('<b>Oops! An error occurred in Google Maps API</b>Distance Matrix service was not successful for the following reason: '+ status);
		}
	}
	
	$('#localization_form').submit(function(e) {
		var code_postal = $(this).find('input').val();
		$('#reset').show();
		if ($.trim(code_postal) == '') { 
			showError(noResultsError);
			return false;
		}
		var service = new google.maps.DistanceMatrixService();
		geocoder.geocode( { 'address': code_postal}, function(result, status) {
			if (status == google.maps.GeocoderStatus.OK) {
				userAddress = result[0].geometry.location;
				var destinations = new Array ();
				for (i in stores) {
					destinations.push(stores[i].latlng); 
				}
				service.getDistanceMatrix ({
					origins: [code_postal],
					destinations: destinations,
					travelMode: google.maps.TravelMode.DRIVING,
					unitSystem: google.maps.UnitSystem.METRIC,
					avoidHighways: false,
					avoidTolls: false
				}, build);

			} else if (status == google.maps.GeocoderStatus.ZERO_RESULTS) {
				showError(noResultsError);
			} else {
				showError('<b>Oops! An error occurred in Google Maps API</b>Geocoding service was not successful for the following reason: '+ status);
			}
		});
		e.preventDefault();
	});
		
	$('#localisation_result').on('click', '.location' , function() {
		var index = $(this).attr('addressIndex');
		bounds = new google.maps.LatLngBounds();
		bounds.extend(stores[index].latlng);
		map.fitBounds(bounds);
		map.setZoom(15);
	});
	
	$('#reset').click(function() {
		$(this).hide().prev('input').val('');
		resetMap(); 
	});
	
	initializeMap();
	
});