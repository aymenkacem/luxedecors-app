$(document).ready(function(){

	// Affichage du titre et de la référence au dessus de la galerie sur mobile.
	$('<div class="title-ref-copy"/>').append($("#product-details").find("h1, .ref").clone()).insertBefore("#gallery-wrap");

	$("#gallery").royalSlider({
	    fullscreen: {
	        enabled: true,
	        // nativeFS: true
	    },
	    imageAlignCenter: false,
	    autoScaleSlider:false,
	    imageScaleMode:"fit",
	    imgWidth: 1280,
	    imgHeight: 700,
	    imageScalePadding: 0,
	    controlNavigation: "thumbnails",
	    slidesSpacing: 0,
	    arrowsNav: false,
	    keyboardNavEnabled: true,
	    globalCaption: false,
	    autoHeight: true,
	    nativeFS: false,
	    thumbs: {
	        arrows: false,
	        spacing: 15,
	        firstMargin: true,
	        fitInViewport: false
	    }
	});

	$("#gallery .rsSlide").addClass("preload");
	var slider = $("#gallery").data("royalSlider");
	slider.slides[0].holder.on("rsAfterContentSet", function() {
	    $("#gallery .rsSlide").removeClass("preload");
	});

	function get_tweets(url) {
	    $.getJSON("https://cdn.api.twitter.com/1/urls/count.json?callback=?&url=" + url,
	        function(data) {
	            $("#tw").html(data.count);
	    });
	}
	function get_likes(url) {
	    $.getJSON("https://graph.facebook.com/?id=" + url,
	        function(data) {
	            $("#fb").html(data.shares ? data.shares : "0");
	    });
	}
	get_tweets(document.location.href);
	get_likes(document.location.href);

	$("#tw, #fb").click(function() {
	    var width  = 575,
	        height = 260,
	        left   = ($(window).width()  - width)  / 2,
	        top    = ($(window).height() - height) / 2,
	        url    = this.href,
	        opts   = "status=1,width="+ width+",height="+height+",top="+top+",left="+left;

	    window.open(url, "share-popup", opts);
	    return false;
	});

	$('#choose-box span').click(function(e) {
		var href_link=$(this).attr('href');
		//var color=$(this).closest('div').hasClass('colors');
		//var value=color?$(this).data('color'):$(this).html();
		$(this).addClass('active').siblings().removeClass('active');
	});

	$('#type_pict').change(function(e) {
		var value=$(this).val();
		$('#laminage_encadrement').val(value);
	});

	$('#quantity .plus').click(function(e) {
		var qty_value=parseInt($('#input-quantity').val())+1;
		$('#input-quantity').val(qty_value);
		quantity = qty_value;
		
		var stocked=parseInt($('#stocked_product').val());
		if(stocked < qty_value){
			$('#add_to_cart').addClass('disabled');
			$('#unavailable_prod').show();
			$('#available_prod').hide();
		}else{
			$('#add_to_cart').removeClass('disabled');
			$('#unavailable_prod').hide();
			$('#available_prod').show();
		}
	});
	$('#quantity .minus').click(function(e) {
		var qty_value=parseInt($('#input-quantity').val())-1;
		if(qty_value>0) {
			$('#input-quantity').val(qty_value);
			quantity = qty_value;
		}else return false;
		
		var stocked=parseInt($('#stocked_product').val());
		if(stocked < qty_value){
			$('#add_to_cart').addClass('disabled');
			$('#unavailable_prod').show();
			$('#available_prod').hide();
		}else{
			$('#add_to_cart').removeClass('disabled');
			$('#unavailable_prod').hide();
			$('#available_prod').show();
		}
	});

	$('#quantity #input-quantity').on('input propertychange',function(){
		if(!$.isNumeric( $('#input-quantity').val())){
			$('#input-quantity').val(1);
			return false;
		}
		var qty_value=parseInt($('#input-quantity').val());
		if(!(qty_value>0)) {
			$('#input-quantity').val(1);
			return false;
		}
		
		var stocked=parseInt($('#stocked_product').val());
		if(stocked < qty_value){
			$('#add_to_cart').addClass('disabled');
			$('#unavailable_prod').show();
			$('#available_prod').hide();
		}else{
			$('#add_to_cart').removeClass('disabled');
			$('#unavailable_prod').hide();
			$('#available_prod').show();
		}
	});
								
	$('#add_to_cart').click(function(e) {
		e.preventDefault();
		//alertify.error('Service encore en maintenance. Désolé pour ce désagrément.');return false;
		if ($(this).hasClass('loading') || $(this).hasClass('disabled')) return;
		var element=$(this);
		element.addClass('loading');
		var href_link=$(this).attr('href');
		var new_value=$('#input-quantity').val();
		var item_price=$('#item_price').val();
		var item_sku=$('#product_ref').val();console.log(item_sku);
		var item_promotion=$('#item_promotion').val();
		var item_variation=$('#item_variation').val();
		var laminage_encadrement=$('#laminage_encadrement').val();
		var price_lam_enc=$('#price_lam_enc').val();

		var new_stock=$('#stocked_product').val()-new_value;
		$('#stocked_product').val(new_stock);
		if(!new_stock){
			$('#add_to_cart').addClass('disabled');
			$('#unavailable_prod').show();
			$('#available_prod').hide();
		}
		$.ajax({
		   type: "POST",
		   data: ({quantity : new_value , item_price : item_price , item_sku : item_sku , item_promotion : item_promotion , item_variation : item_variation, laminage_encadrement : laminage_encadrement, price_lam_enc : price_lam_enc}),
		   url: ''+href_link,
		   dataType: "json",
		   success: function(response){
		   		$.cartWidgetRefresh(response.nbre);
		   		element.removeClass('loading');
		   		alertify.success(success_message);
		   		$('#input-quantity').val(1);
		   }
		});
	});
	
	$('#add_to_wishlist').click(function(e) {
		var href_link=$(this).attr('href');
		var role_link=$(this).attr('role');
		if(role_link=='true'){
			$.ajax({
				type: "POST",
				url: ''+href_link,
				//dataType: "json",
				success: function(response){
					//$('#cart_sum').html(response);
				}
			});
			e.preventDefault();
		}
	});
	
});

function wishlist_delete_item(wish_item_id)
{
	lien=base_url+site_lang+'/payment/wish_list/delete/'+wish_item_id;
	$.ajax({
		type: "POST",
		url: ''+lien,
		success: function(msg){
		   $('#nbre_wishlist').html(msg);
		}
	});
}