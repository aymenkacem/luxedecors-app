// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
angular.module('app', ['ionic', 'app.controllers', 'app.routes', 'app.services', 'app.directives'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs).
    // The reason we default this to hidden is that native apps don't usually show an accessory bar, at
    // least on iOS. It's a dead giveaway that an app is using a Web View. However, it's sometimes
    // useful especially with forms, though we would prefer giving the user a little more room
    // to interact with the app.

    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(false);
    }
    if (window.StatusBar) {
      // Set the statusbar to use the default style, tweak this to
      // remove the status bar on iOS or change it to use white instead of dark colors.
      StatusBar.styleDefault();
      StatusBar.overlaysWebView(false);
    }

    if (window.Connection) {
      document.addEventListener("offline", function(){
        navigator.notification.alert('Veuillez vérifier votre connexion Internet et réessayer', function() {
          window.location.reload();
        }, "Aucune connexion Internet", "Réessayer");
      }, false);
    }

    if(navigator.splashscreen) {
      setTimeout(function() {
          navigator.splashscreen.hide();
      }, 300);
    }
  });
})
.constant('$ionicLoadingConfig', {
  template: '<ion-spinner icon="spiral" class="spinner-light"></ion-spinner>',
  hideOnStateChange: true,
  noBackdrop: true
})
// .constant('SERVER_PATH','http://localhost:8100/api')
.constant('SERVER_PATH','https://www.luxedecors.com.tn/api');
