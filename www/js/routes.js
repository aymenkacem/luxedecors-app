angular.module('app.routes', ['app', 'satellizer'])

.config(function($stateProvider, $urlRouterProvider, $ionicConfigProvider, $authProvider) {

  $authProvider.authHeader = 'Auth';
  // $authProvider.loginUrl = 'http://localhost:8100/api/auth/login';
  $authProvider.loginUrl = 'https://www.luxedecors.com.tn/api/auth/login';

  function _redirectIfNotAuthenticated($q, $state, $auth, $timeout) {
    var defer = $q.defer();
    if($auth.isAuthenticated()) {
      defer.resolve(); /* (3) */
    } else {
      $timeout(function () {
        $state.go('home'); /* (4) */
      });
      defer.reject();
    }
    return defer.promise;
  }


  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $ionicConfigProvider.views.maxCache(1);
  $ionicConfigProvider.scrolling.jsScrolling(false);
  $ionicConfigProvider.views.swipeBackEnabled(false);

  $stateProvider

    .state('home', {
      cache: false,
      url: '/home',
      templateUrl: 'templates/home.html',
      controller: 'HomeCtrl',
      resolve: {
        home: function(appService, $ionicLoading) {
          $ionicLoading.show();
          return appService.home();
        }
      }
    })
    .state('productList', {
      url: '/product/list/:id/:page',
      templateUrl: 'templates/product-list.html',
      controller: 'ProductListCtrl',
      params: {
        page: null
      },
      resolve: {
        productList: function($stateParams, productService, $ionicLoading) {
          $ionicLoading.show();
          var page = $stateParams.page || 0;
          return productService.getList($stateParams.id, page);
        }
      }
    })
    .state('search', {
      url: '/search/:s',
      templateUrl: 'templates/search.html',
      controller: 'SearchCtrl',
      resolve: {
        productList: function($stateParams, productService, $ionicLoading) {
          $ionicLoading.show();
          return productService.search({product_ref: $stateParams.s, page: 0});
        }
      }
    })
    .state('productPromotion', {
      url: '/productPromotion/:categ',
      templateUrl: 'templates/search.html',
      controller: 'ProductPromotionCtrl',
      params: {
        page: null,
        categ: null
      },
      resolve: {
        productList: function($stateParams, productService, $ionicLoading) {
          $ionicLoading.show();
          return productService.productPromotion({categorie_id: $stateParams.categ, page: 0});
        }
      }
    })
    .state('productDetail', {
      url: '/product/detail/:id',
      templateUrl: 'templates/product-detail.html',
      controller: 'ProductDetailCtrl',
      resolve: {
        productDetail: function($stateParams, productService, $ionicLoading) {
          $ionicLoading.show();
          return productService.getDetail($stateParams.id);
        }
      }
    })
    .state('cart', {
      url: '/cart',
      templateUrl: 'templates/cart.html',
      controller: 'CartCtrl',
      resolve: {
        cartItems: function(paymentService, $ionicLoading) {
          $ionicLoading.show();
          return paymentService.cartItems(localStorage.getItem('cart'));
        }
      }
    })
    .state('checkout', {
      url: '/checkout',
      templateUrl: 'templates/checkout.html',
      resolve: {
        redirectIfNotAuthenticated: _redirectIfNotAuthenticated
      },
      controller: 'CheckoutCtrl'
    })
    .state('billing', {
      url: '/billing',
      templateUrl: 'templates/billing.html',
      resolve: {
        redirectIfNotAuthenticated: _redirectIfNotAuthenticated
      },
      controller: 'BillingCtrl'
    })
    .state('payment', {
      url: '/payment/:billing_id',
      templateUrl: 'templates/payment.html',
      resolve: {
        redirectIfNotAuthenticated: _redirectIfNotAuthenticated
      },
      controller: 'PaymentCtrl'
    })
    .state('dashboard', {
      url: '/dashboard',
      templateUrl: 'templates/dashboard.html',
      controller: 'DashboardCtrl',
      resolve: {
        redirectIfNotAuthenticated: _redirectIfNotAuthenticated,
        dashboard: function(paymentService, $ionicLoading) {
          $ionicLoading.show();
          return paymentService.dashboard();
        }
      }
    })
    .state('billingList', {
      url: '/billingList',
      templateUrl: 'templates/billing-list.html',
      controller: 'BillingListCtrl',
      resolve: {
        redirectIfNotAuthenticated: _redirectIfNotAuthenticated,
        billingList: function(paymentService, $ionicLoading) {
          $ionicLoading.show();
          return paymentService.billingList();
        }
      }
    })
    .state('billingView', {
      url: '/billingView/:id',
      templateUrl: 'templates/billing-view.html',
      resolve: {
        redirectIfNotAuthenticated: _redirectIfNotAuthenticated,
        billingView: function($stateParams, paymentService, $ionicLoading) {
          $ionicLoading.show();
          return paymentService.billingView($stateParams.id);
        }
      },
      controller: 'BillingViewCtrl'
    })
    .state('contact', {
      url: '/contact',
      templateUrl: 'templates/contact.html',
      controller: 'ContactCtrl'
    })
    .state('faq', {
      url: '/faq',
      templateUrl: 'templates/faq.html',
      controller: 'FaqCtrl'
    })
    .state('stores', {
      url: '/stores',
      templateUrl: 'templates/stores.html',
      controller: 'StoresCtrl'
    })
    .state('terms-and-conditions', {
      url: '/terms-and-conditions',
      templateUrl: 'templates/terms-and-conditions.html',
      controller: 'TermsAndConditionsCtrl'
    })
    .state('page', {
      url: '/page/:slug',
      templateUrl: 'templates/page.html',
      controller: 'PageCtrl'
    })
    ;

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/home');

});
