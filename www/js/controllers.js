angular.module('app.controllers', [])

.controller('AppCtrl', function($scope, $rootScope, appService, productService, $state, $window, $auth, $ionicLoading, $ionicPopup) {
  $rootScope.formData = {
    signin: {},
    signup: {},
    lostpassword: {},
    profile: {},
    address: {},
    newsletter: {},
    checkout: {},
    searchInput: ''
  };
  $rootScope.searchForm = {
    s: '',
    ajaxResult: {}
  };
  $rootScope.authenticated = false;

  $rootScope.convertUrlToRoute = function(url) {
    var re = new RegExp("project\/product\/index\/([1-9]+)");
    if (url.match(re)) {
      var id = re.exec(url);
      $state.go('productList', {id: id[1]});
      return;
    }
    var re = new RegExp("project\/product\/view_\/([1-9]+)");
    if (url.match(re)) {
      var id = re.exec(url);
      $state.go('productDetail', {id: id[1]});
      return;
    }
    var re = new RegExp("module\/faq\/index");
    if (url.match(re)) {
      $state.go('faq');
      return;
    }
    var re = new RegExp("module\/contact\/index");
    if (url.match(re)) {
      $state.go('contact');
      return;
    }
    var re = new RegExp("project\/stores\/index");
    if (url.match(re)) {
      $state.go('stores');
      return;
    }

    $state.go('home');
    return;
  }

  function init() {
    if ($auth.isAuthenticated()) {
      $rootScope.authenticated = true;
      $rootScope.currentUser = angular.fromJson(localStorage.getItem('user')) || {};
    }

    $rootScope.menuActive = false;
    $rootScope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams){
      $rootScope.menuActive = false;
    });

    $rootScope.cart = angular.fromJson(localStorage.getItem('cart')) || [];
    $rootScope.$watch('cart', function () {
      localStorage.setItem('cart', angular.toJson($rootScope.cart));
    }, true);

    $rootScope.coupon = angular.fromJson(localStorage.getItem('coupon')) || {
      type: null,
      code: null,
      discount: null,
      minPurchase: null,
      valid: null,
      error: null
    };
    $rootScope.$watch('coupon', function () {
      localStorage.setItem('coupon', angular.toJson($rootScope.coupon));
    }, true);

    $rootScope.checkoutInfo = angular.fromJson(localStorage.getItem('checkoutInfo')) || {};
    $scope.$watch('checkoutInfo', function () {
      localStorage.setItem('checkoutInfo', angular.toJson($scope.checkoutInfo));
    }, true);

    appService.getSettings().then(function(response){
      $rootScope.menu = response.data.menu;
      $rootScope.sidebox = response.data.sidebox;
      $rootScope.footerServices = response.data.footerServices;
      $rootScope.year = response.data.year;

      $rootScope.shipping_fee = parseInt(response.data.shipping_fee);
      $rootScope.free_transport = parseInt(response.data.free_transport);
      $rootScope.mobile = response.data.mobile;
      $rootScope.tel = response.data.tel;
    });
  }
  init();

  // Helper functions

  $rootScope.parseFloat = parseFloat;
  $rootScope.parseInt = parseInt;

  $rootScope.menuChunk = function(menu) {
    var result = [];
    if (menu.length < 6) {
      result.push(menu);
      return result;
    }

    var length = menu.length;
    var step = 5;
    if (length < 9) step--;
    if (length < 7) step--;
    for (var i = 0;i < length;i+=step)
      result.push(menu.slice(i,i+step));

    return result;
  }

  $rootScope.menuLevel = 0;

  $rootScope.toggleCollapseMenu = function() {
    $rootScope.menuActive = ! $rootScope.menuActive;
    $rootScope.menuLevel = 0;
    $(".main-menu.collapse").attr("style", "").find("ul").hide();
  }

  $rootScope.navSubCateg = function($event) {
    $event.stopPropagation();
	  $event.preventDefault();
    $($event.target).parent().siblings("ul").show();
    $rootScope.menuLevel -= 100;
    $(".main-menu.collapse").transition({x: $rootScope.menuLevel + '%'});
  }

  $rootScope.navSupCateg = function($event) {
    $rootScope.menuLevel += 100;
    var elem = $($event.target).parent("ul");
    $(".main-menu.collapse").transition({x: $rootScope.menuLevel + '%'}, function () {
        elem.hide();
    });
  }

  $rootScope.openBrowser = function(url) {
    var ref = window.open(url, '_blank', 'location=no');
  }

  $rootScope.openViewInNewTab = function(route) {
    var url = $state.href(route);
    window.open(url, '_blank', 'location=no');
  }

  $rootScope.thumb = function (url, name) {
    return url.replace(/\.\w+$/i, function(s) {
      return name + s;
    });
  }

  $rootScope.hasPromo = function(product) {
    var promotion = false;
    var date = new Date();
    var promoFrom = new Date(product.price_special_from);
    var promoTo = new Date(product.price_special_to);
    if (product.promotion > 0 && date > promoFrom && date < promoTo) {
      promotion = true;
    }
    return promotion;
  }

  $rootScope.total = function(cartItems, discount) {
    var total = 0;
    for (i in cartItems) {
      total += (cartItems[i].promotion_value > 0 ? cartItems[i].promo_price : cartItems[i].price) * cartItems[i].quantity;
    }
    if (discount != undefined) {
      if ($rootScope.coupon.type == 2) {
        total=(total*(100-Math.abs($rootScope.coupon.discount))/100)
      } else {
        total-=Math.abs($rootScope.coupon.discount);
      }
    }
    return total;
  }

  $rootScope.amountFormat = function(number) {
  	if(number<0) number=0;
    return parseFloat(number).toFixed(3).toString().replace(/\.000$/, '') + " DT";
  }

  $rootScope.getCities = function() {
    return {
      states: {"301":"Ariana","302":"Béja","303":"Ben Arous","304":"Bizerte","305":"Gabès","306":"Gafsa","307":"Jendouba","308":"Kairouan","309":"Kasserine","310":"Kebili","311":"Kef","312":"Mahdia","313":"Manouba","314":"Medenine","315":"Monastir","316":"Nabeul","317":"Sfax","318":"Sidi Bouzid","319":"Siliana","320":"Sousse","321":"Tataouine","322":"Tozeur","323":"Tunis","324":"Zaghouan"},
      cities: {"301":["Ariana Ville","Ettadhamen","Kala\u00e2t El Andalous","La Soukra","Mnihla","Raoued","Sidi Thabet"],"302":["Amdoun","B\u00e9ja Nord","B\u00e9ja Sud","Goubellat","Medjez el-Bab","Nefza","T\u00e9boursouk","Testour","Thibar"],"303":["Ben Arous","Boumhel","El Mourouj","Ezzahra","Fouchana","Hammam Chott","Hammam Lif","Mohamedia","Medina Jedida","M\u00e9grine","Mornag","Rad\u00e8s"],"304":["Bizerte Nord","Bizerte Sud","Djoumime","El Alia","Ghar El Melh","Ghezala","Mateur","Menzel Bourguiba","enzel Jemil","Ras Jebel","Sejenane","Tinja","Utique","Zarzouna"],"305":["Gab\u00e8s M\u00e9dina","Gab\u00e8s Ouest","Gab\u00e8s Sud","Ghanouch","El Hamma","Matmata","Mareth","Menzel El Habib","M\u00e9touia","Nouvelle Matmata"],"306":["Belkhir","El Guettar","El Ksar","Gafsa Nord","Gafsa Sud","Mdhila","M\u00e9tlaoui","Oum El Araies","Redeyef","Sidi A\u00efch","Sened"],"307":["Ain Draham","Balta-Bou Aouane","Bou Salem","Fernana","Ghardimaou","Jendouba","endouba Nord","Oued Meliz","Tabarka"],"308":["Bouhajla","Chebika","Echrarda","El Al\u00e2a","El Ouslatia","Haffouz","Hajeb El Ayoun","Kairouan Nord","Kairouan Sud","Nasrallah","Sbikha"],"309":["Djedeliane","El Ayoun","Ezzouhour","F\u00e9riana","Foussana","Hassi Ferid","Hidra","Kasserine Nord","Kasserine Sud","Majel Bel Abb\u00e8s","Sbe\u00eftla","Sbiba","Thala"],"310":["Douz Nord","Douz Sud","Faouar","K\u00e9bili Nord","K\u00e9bili Sud","Souk Lahad"],"311":["Dahmani","Djerissa","El Ksour","Es-Sers","Kal\u00e2at Khasbah","Kal\u00e2at Snan","Kef Est","Kef Ouest","Nebeur","Sakiet Sidi Youssef","Tajerouine"],"312":["Bou Merd\u00e8s","Chebba","Chorbane","El Jem","Essouassi","Hebira","Ksour Essef","Mahdia","Melloul\u00e8che","Ouled Chamekh","Sidi Alouane"],"313":["Borj El Amri","Djedeida","Douar Hicher","El Battan","Manouba","Mornaguia","Oued Ellil","Tebourba"],"314":["Ben Gardane","Beni Khedech","Djerba - Ajim","Djerba-Houmt Souk","Djerba - Midoun","M\u00e9denine Nord","M\u00e9denine Sud","Sidi Makhloulf","Zarzis"],"315":["Bekalta","Bembla","Beni Hassen","Jemmal","Ksar Hellal","Ksibet el-M\u00e9diouni","Moknine","Monastir","Ouerdanine","Sahline","Sayada-Lamta-Bou Hajar","T\u00e9boulba","Z\u00e9ramdine"],"316":["B\u00e9ni Khalled","B\u00e9ni Khiar","Bou Argoub","Dar Ch\u00e2abane El Fehri","El Haouaria","El Mida","Grombalia","Hammam Ghez\u00e8ze","Hammamet","K\u00e9libia","Korba","Menzel Bouzelfa","Menzel Temime","Nabeul","Soliman","Takelsa"],"317":["Agareb","Bir Ali Ben Khalifa","Jebiniana","El Amra","El Hencha","Ghraiba","Kerkennah","Mahr\u00e8s","Menzel Chaker","Sakiet Edda\u00efer","Sakiet Ezzit","Sfax Ouest","Sfax Sud","Sfax Ville","Skhira","Thyna"],"318":["Bir El Hafey","Cebbala Ouled Asker","Jilma","Meknassy","Menzel Bouzaiane","Mezzouna","Ouled Haffouz","Regueb","Sidi Ali Ben Aoun","Sidi Bouzid Est","Sidi Bouzid Ouest","Souk Jedid"],"319":["Bargou","Bou Arada","Sidi Bou Rouis","El Aroussa","El Krib","Rouhia","Ga\u00e2four","Kesra","Makthar","Siliana Nord","Siliana Sud"],"320":["Akouda","Bouficha","Enfidha","Hammam Sousse","Hergla","Kala\u00e2 Kebira","Kala\u00e2 Sghira","Kondar","Msaken","Sidi Bou Ali","Sidi El H\u00e9ni","Sousse Jawhara","Sousse M\u00e9dina","Sousse Riadh","Sousse Sidi Abdelhamid","Zaouit-Ksibat Thrayett"],"321":["Bir Lahmar","Dehiba","Ghomrassen","Remada","Sm\u00e2r","Tataouine Nord","Tataouine Sud"],"322":["Degache","Hazoua","Nefta","Tameghza","Tozeur"],"323":["Bab El Bhar","Bab Souika","Carthage","Cit\u00e9 El Khadra","Djebel Jelloud","El Kabaria","El Menzah","El Omrane","El Omrane sup\u00e9rieur","El Ouardia","Ettahrir","Ezzouhour","Hra\u00efria","La Goulette","La Marsa","Le Bardo","Le Kram","M\u00e9dina","S\u00e9joumi","Sidi El B\u00e9chir","Sidi Hassine"],"324":["Bir Mchergua","El Fahs","En-Nadhour","Ez-Zeriba","Saouaf","Zaghouan"]}
    }
  }

  $rootScope.doRefresh = function() {
    $state.reload();
    $scope.$broadcast('scroll.refreshComplete');
  }

  $rootScope.cartWidgetRefresh = function (qty) {
    if(qty === undefined) qty=parseInt($("#cart_sum").html());
    else {
      if(qty != parseInt($("#cart_sum").html())) $("#cart_sum").html(qty).parent().transition({ scale: 1.2 }, 200, 'in').transition({ scale: 1 });
    }
    $("#cart-link").toggleClass('highlight', qty>0).toggleClass('two-digits', qty>9)
  }
  $rootScope.cartWidgetRefresh();

  $rootScope.doLogin = function(formData) {
    var credentials = {email: formData.email, password: formData.password};
    $ionicLoading.show();
    $auth.login(credentials).then(function(response) {
      localStorage.setItem('user', angular.toJson(response.data.user));
      $window.location.reload();
    }).catch(function(response){
      $rootScope.formData.signin.errors = response.data;
      $ionicLoading.hide();
    });
  }

  $rootScope.googleSignin = function() {
    $ionicLoading.show();
    window.plugins.googleplus.login(
        {
          'webClientId': '729739307904-8k6pa7ptq88pj4sdoubmpbjhe48iljil.apps.googleusercontent.com', // optional clientId of your Web application from Credentials settings of your project - On Android, this MUST be included to get an idToken. On iOS, it is not required.
          'offline': true // optional, but requires the webClientId - if set to true the plugin will also return a serverAuthCode, which can be used to grant offline access to a non-Google server
        },
        function (obj) {
          $auth.login({accessToken: obj, googleSignin: true}).then(function(response) {
            localStorage.setItem('user', angular.toJson(response.data.user));
            $window.location.reload();
          });
        },
        function (msg) {
          $ionicLoading.hide();
        }
    );
  }

  $rootScope.facebookSignIn = function() {
    if (!window.cordova || window.cordova.platformId == "browser") {
      facebookConnectPlugin.browserInit('1488292514719695');
    }
    $ionicLoading.show();

    facebookConnectPlugin.getLoginStatus(function(success){
      if(success.status === 'connected'){
        // The user is logged in and has authenticated your app, and response.authResponse supplies
        // the user's ID, a valid access token, a signed request, and the time the access token
        // and signed request each expire

        $auth.login({accessToken: success.authResponse.accessToken, facebookLogin: true}).then(function(response) {
          localStorage.setItem('user', angular.toJson(response.data.user));
          $window.location.reload();
        });

      } else {
        // If (success.status === 'not_authorized') the user is logged in to Facebook,
				// but has not authenticated your app
        // Else the person is not logged into Facebook,
				// so we're not sure if they are logged into this app or not.

				// Ask the permissions you need. You can learn more about
				// FB permissions here: https://developers.facebook.com/docs/facebook-login/permissions/v2.4
        facebookConnectPlugin.login(['email', 'public_profile'], function(response) {
            if (response.status === 'connected') {
                $auth.login({accessToken: response.authResponse.accessToken, facebookLogin: true}).then(function(response) {
                  localStorage.setItem('user', angular.toJson(response.data.user));
                  $window.location.reload();
                });
            } else {
                facebookConnectPlugin.showDialog(["email"],function(response){});
                $ionicLoading.hide();
            }

        },function(response){
            $ionicLoading.hide();
            $state.go('home');
        });
      }
    });

  };

  $rootScope.logout = function () {
    $auth.logout();
    $rootScope.cart = [];
    $window.location.reload();
  }

  $rootScope.signup = function (formData) {
    $ionicLoading.show();
    appService.signup(formData).then(function(response){
      if (response.data.success) {
        var credentials = {email: formData.email, password: formData.password};
        $rootScope.doLogin(credentials);
      } else {
        $ionicLoading.hide();
        $rootScope.formData.signup.errors = response.data.errors;
      }
    });
  }

  $rootScope.retrievePass = function (email) {
    appService.retrievePass(email).then(function(response){
      if (response.data.success) {
        $.fancybox.close(true);
      }
    }).catch(function(response){
      $scope.formData.errors = response.data;
    });
  }

  $rootScope.newsletter = function(email) {
    $('#newsletter-form .error').hide();
    $('#newsletter-form .loading').fadeIn('fast');
    appService.newsletter(email).then(function(response){
      response = response.data;
      if(response.valid){
        $('#newsletter-form .loading').fadeOut('fast', function(){
            $('#newsletter-form').hide();
            $('#popin-newsletter .success-msg').show();
          });
        }else{
          $('#newsletter-form .error').hide();
          if (typeof response.message != 'undefined') {
            $('#newsletter-form .error').find('span').text(response.message);
            $('#newsletter-form .error').css('display','inline-block');
          }
        }
        $('#newsletter-form .loading').fadeOut('fast');
    });
  }
})
.controller('SearchBoxCtrl', function($scope, $rootScope, SERVER_PATH) {
  $scope.active = false;
  $scope.preventBlur = false;

  $scope.enableSearch = function() {
    $scope.active = true;
    if ($rootScope.searchForm.s != "")
      angular.element(document.getElementsByClassName("clear-search")).css('display','block');
  }

  $scope.clearSearch = function() {
    var $this = $(this);
    var parent = $this.parent('#search-box');
    $rootScope.searchForm.s = '';
    $scope.preventBlur = true;
    angular.element(document.getElementsByClassName("clear-search")).css('display','none');
    angular.element(document.getElementsByClassName("ajax-result")).css('display', 'none');
  }

  angular.element(document.getElementsByClassName("search-input")).on('blur', function(){
    var that = this;
    setTimeout(function() {
      if ($scope.preventBlur) {
        that.focus();
        return $scope.preventBlur = false;
      }

      $scope.active = false;
      angular.element(document.getElementsByClassName("clear-search")).css('display','none');
      angular.element(document.getElementsByClassName("ajax-result")).css('display', 'none');
    }, 100);
  });

  var xhr = $.ajax();
  var timeoutId = 0;
  $(".search-input").on('input propertychange',function(){
    clearTimeout(timeoutId);
    var $this = $(this);
    timeoutId = setTimeout(function () {
      if( $this.val()){
        angular.element(document.getElementsByClassName("clear-search")).addClass("loading icon-spin").css('display','block');
        xhr.abort();
        xhr=$.ajax({
          type: 'POST',
          url: SERVER_PATH + '/search',
          data: {product_ref: $rootScope.searchForm.s, ajax_request: 1},
          success: function(data) {
            angular.element(document.getElementsByClassName("clear-search")).removeClass("loading icon-spin");
            angular.extend($rootScope.searchForm.ajaxResult, data);
            $scope.$apply();
            angular.element(document.getElementsByClassName("ajax-result")).css('display', 'block');
          }
        });
      }
    }, 650);
  });

})
.controller('HomeCtrl', function($scope, $rootScope, home, appService) {
  $scope.rsOptions = {
    arrowsNavAutoHide: false,
    keyboardNavEnabled: true,
    controlsInside: false,
    imageScaleMode: 'fill',
    autoScaleSlider: true,
    autoScaleSliderWidth: 870,
    autoScaleSliderHeight: 400,
    slidesSpacing: 0,
    loop: true,
    loopRewind: true,
    autoPlay: {
        // autoplay options go gere
        enabled: true,
        pauseOnHover: true,
        delay: 8000
    }
  };

  $scope.home = home.data;

  setTimeout(function() {
    $('.products-carousel').find('img').each(function( index ) {
      var img=$(this);
      img.data("src",img.attr("src")).attr("src","").hide();
    });

    var productCarousel = $('.products-carousel').touchCarousel({
      itemsPerMove: 1,
      snapToItems: true,
      lockAxis: false,
      scrollbar: false,
      onAnimComplete: function() {
        loadCarouselItem(this);
      }
    }).each(function( index ) {
      loadCarouselItem($(this).data("touchCarousel"));
    });

    //Load images of visible items in Products Carousels
    function loadCarouselItem (carousel) {
      var index=carousel.getCurrentId();
      var firstVisible=carousel.items[index];
      var visible=carousel.items[0]["item"].parents(".touchcarousel-wrapper").width();
      for (var i = index; i < carousel.numItems; i++) {
        var element=carousel.items[i];
        if (element["item"].data("loaded")!="true" && element["posX"]-firstVisible["posX"]<=visible) {
          element["item"].find("img").each(function( index ) {
            var img=$(this);
            if (img.data("src")) {
              img.load(function() { $(this).hide().fadeIn("slow"); }).attr("src",img.data("src"));
            }
          });
          element["item"].data("loaded","true");
        }
      };
    }
  }, 500);

})

.controller('ProductListCtrl', function($scope, $stateParams, productList, productService) {
  $scope.productList = productList.data;

  $scope.id = $stateParams.id;
  $scope.nextPage = $scope.productList.next_page;

  $scope.loadMore = function() {
    return productService.getList($scope.id, $scope.nextPage).then(function(response) {
      $scope.productList.product_list = $scope.productList.product_list.concat(response.data.product_list);
      $scope.nextPage = response.data.next_page;
      $scope.$broadcast('scroll.infiniteScrollComplete');
    });
  };
})

.controller('SearchCtrl', function($scope, $stateParams, productList, productService) {
  $scope.productList = productList.data;
  $scope.nextPage = $scope.productList.next_page;

  $scope.loadMore = function() {
    return productService.search({product_ref: $stateParams.s, page: $scope.nextPage}).then(function(response) {
      $scope.productList.product_list = $scope.productList.product_list.concat(response.data.product_list);
      $scope.nextPage = response.data.next_page;
      $scope.$broadcast('scroll.infiniteScrollComplete');
    });
  };
})

.controller('ProductPromotionCtrl', function($scope, $stateParams, productList, productService) {
  $scope.productList = productList.data;
  $scope.nextPage = $scope.productList.next_page;

  $scope.loadMore = function() {
    return productService.productPromotion({categ: $stateParams.categ, page: $scope.nextPage}).then(function(response) {
      $scope.productList.product_list = $scope.productList.product_list.concat(response.data.product_list);
      $scope.nextPage = response.data.next_page;
      $scope.$broadcast('scroll.infiniteScrollComplete');
    });
  };
})

.controller('ProductDetailCtrl', function($scope, $rootScope, $stateParams, $filter, productService, $ionicHistory, productDetail) {
  $scope.productDetail = productDetail.data;
  $scope.productDetail.product._stocked = $scope.productDetail.product.stocked;

  $scope.promotion = $rootScope.hasPromo($scope.productDetail.product);

  var cartProd = $filter('filter')($rootScope.cart, {id: $scope.productDetail.product.product_id});
  if (cartProd.length > 0) {
    $scope.productDetail.product.stocked -= cartProd[0].quantity;
  }

  $scope.formData = {};
  $scope.quantity = 1;
  $scope.rsOptions = {
    fullscreen: {
      enabled: true,
      // nativeFS: true
    },
    imageAlignCenter: false,
    autoScaleSlider:false,
    imageScaleMode:"fit",
    imgWidth: 1280,
    imgHeight: 700,
    imageScalePadding: 0,
    controlNavigation: "thumbnails",
    slidesSpacing: 0,
    arrowsNav: false,
    keyboardNavEnabled: true,
    globalCaption: false,
    autoHeight: true,
    nativeFS: false,
    thumbs: {
      arrows: false,
      spacing: 15,
      firstMargin: true,
      fitInViewport: false
    }
  };

  setTimeout(function() {
    $('.products-carousel').touchCarousel({
      itemsPerMove: 1,
      snapToItems: true,
      lockAxis: false,
      scrollbar: false,
      onAnimComplete: function() {
        loadCarouselItem(this);
      }
    }).each(function( index ) {
      loadCarouselItem($(this).data("touchCarousel"));
    });

    //Load images of visible items in Products Carousels
    function loadCarouselItem (carousel) {
      var index=carousel.getCurrentId();
      var firstVisible=carousel.items[index];
      var visible=carousel.items[0]["item"].parents(".touchcarousel-wrapper").width();
      for (var i = index; i < carousel.numItems; i++) {
        var element=carousel.items[i];
        if (element["item"].data("loaded")!="true" && element["posX"]-firstVisible["posX"]<=visible) {
          element["item"].find("img").each(function( index ) {
            var img=$(this);
            if (img.data("src")) {
              img.load(function() { $(this).hide().fadeIn("slow"); }).attr("src",img.data("src"));
            }
          });
          element["item"].data("loaded","true");
        }
      };
    }
  }, 500);

  $scope.goBack = function() {
    $ionicHistory.goBack();
  };

  $scope.increment = function() {
    $scope.quantity ++;
  }

  $scope.decrement = function() {
    if ($scope.quantity == 1) {
      return;
    }
    $scope.quantity --;
  }

  $scope.addToCart = function() {
    var cartProd = $filter('filter')($rootScope.cart, {id: $scope.productDetail.product.product_id});
    if (cartProd.length > 0) {
      var updatedCart = [];
      angular.forEach($rootScope.cart, function(value) {
        if (value.id == $scope.productDetail.product.product_id) {
          value.quantity += $scope.quantity;
        }
        this.push(value);
      }, updatedCart);
      $rootScope.cart = updatedCart;
    } else {
      var product = {
        id: $scope.productDetail.product.product_id,
        quantity: $scope.quantity
      }
      $rootScope.cart.push(product);
    }

    $scope.productDetail.product.stocked -= $scope.quantity;
    $scope.quantity = 1;
    $rootScope.cartWidgetRefresh($rootScope.cart.length);
    alertify.success('Article ajouté au panier');
  }
})

.controller('CartCtrl', function($rootScope, $scope, $filter, cartItems, paymentService, $ionicLoading) {
  cartItems = cartItems.data;

  angular.forEach($rootScope.cart, function(value, key) {
    var cartItem = $filter('filter')(cartItems, {product_id: value.id});
    if (cartItem.length > 0) {
      $rootScope.cart[key].title = cartItem[0].title;
      $rootScope.cart[key].price = parseFloat(cartItem[0].price);
      $rootScope.cart[key].ref = cartItem[0].product_ref;
      $rootScope.cart[key].image = cartItem[0].image;
      $rootScope.cart[key].promotion_value = cartItem[0].promotion_value;
      $rootScope.cart[key].promo_price = cartItem[0].promo_price;
      $rootScope.cart[key].stocked = cartItem[0].stocked;
    } else {
      $rootScope.cart.splice(key, 1);
    }
  });

  $scope.remove = function(id) {
    var updatedCart = [];
    angular.forEach($rootScope.cart, function(value) {
      if (value.id != id) {
        this.push(value);
      }
    }, updatedCart);
    $rootScope.cart = updatedCart;
  }

  $scope.validateCoupon = function() {
    $ionicLoading.show();
    paymentService.validateCoupon($scope.formData.coupon_code).then(function(response) {
      if (response.data.success) {
        $rootScope.coupon.type = response.data.coupon.type_discount;
        $rootScope.coupon.code = response.data.coupon.card_discount_code;
        $rootScope.coupon.discount = response.data.coupon.card_discount;
        $rootScope.coupon.minPurchase = response.data.coupon.min_purchase;
        $rootScope.coupon.valid = true;
      } else {
        $rootScope.coupon.error = response.data.error;
        $rootScope.coupon.valid = false;
      }
    }).finally(function(){
      $ionicLoading.hide();
    });
  }

  $scope.removeCoupon = function() {
    $rootScope.coupon = {
      type: null,
      code: null,
      discount: null,
      minPurchase: null,
      valid: null,
      error: null
    };
  }
})

.controller('CheckoutCtrl', function($scope, $rootScope, $state, paymentService, $ionicLoading) {
  $scope.formData.checkout = {
    'first_name' : $rootScope.currentUser.first_name,
    'last_name' : $rootScope.currentUser.last_name,
    'company' : $rootScope.currentUser.company,
    'tax_number' : $rootScope.currentUser.tax_number,
    'address' : $rootScope.currentUser.address,
    'state' : $rootScope.currentUser.state,
    'city' : $rootScope.currentUser.city,
    'postal_code' : $rootScope.currentUser.postal_code,
    'phone' : $rootScope.currentUser.phone,
    'mobile' : $rootScope.currentUser.mobile
  };

  $scope.processCheckout = function() {
    $ionicLoading.show();
    paymentService.processCheckout($scope.formData.checkout).then(function(response) {
      if (response.data.success) {
        $rootScope.checkoutInfo = $scope.formData.checkout;
        $state.go('billing');
      } else {
        $scope.formData.checkout.errors = response.data.errors;
      }
    }).finally(function(){
      $ionicLoading.hide();
    });
  }
})

.controller('BillingCtrl', function($rootScope, $scope, $state, $ionicLoading, paymentService ,SERVER_PATH) {
  $scope.formData.paymentMethod = 'delivery';
  $scope.confirmCheckout = function() {
    $ionicLoading.show();
    paymentService.confirmCheckout($scope.formData.paymentMethod, $rootScope.checkoutInfo, $rootScope.cart, ($rootScope.coupon.valid && parseFloat($rootScope.coupon.minPurchase) <= $rootScope.total($rootScope.cart)) ? $rootScope.coupon.code : undefined).then(function(response) {
      if ($scope.formData.paymentMethod == 'sps') {
        var ref = window.open(SERVER_PATH + '/gpgCheckout/' + response.data.billing_id, '_blank', 'location=no');
        ref.addEventListener('loadstop', function(event) {
          if (event.url == 'https://www.gpgcheckout.com/payement/erreur/209') {
            ref.close();
          }
        });
        ref.addEventListener('exit', function(event) {
          $state.go('payment', {billing_id: response.data.billing_id});
        });
      } else {
        $state.go('payment', {billing_id: response.data.billing_id});
      }
    }).finally(function(){
      $ionicLoading.hide();
    });
  }
})

.controller('PaymentCtrl', function($scope, $rootScope, $stateParams) {
  $rootScope.cart = [];
})

.controller('DashboardCtrl', function($rootScope, $scope, $stateParams, $ionicLoading, dashboard, appService) {
  $scope.dashboard = dashboard.data;
  $rootScope.formData.profile.first_name = $rootScope.currentUser.first_name;
  $rootScope.formData.profile.last_name = $rootScope.currentUser.last_name;
  $rootScope.formData.profile.email = $rootScope.currentUser.email;
  $rootScope.formData.profile.tax_number = $rootScope.currentUser.tax_number;
  $rootScope.formData.profile.newsletter = $rootScope.currentUser.newsletter;

  $rootScope.formData.address.company = $rootScope.currentUser.company;
  $rootScope.formData.address.address = $rootScope.currentUser.address;
  $rootScope.formData.address.state = $rootScope.currentUser.state;
  $rootScope.formData.address.postal_code = $rootScope.currentUser.postal_code;
  $rootScope.formData.address.city = $rootScope.currentUser.city;
  $rootScope.formData.address.state = $rootScope.currentUser.state;
  $rootScope.formData.address.phone = $rootScope.currentUser.phone;
  $rootScope.formData.address.fax = $rootScope.currentUser.fax;

  $scope.editProfile = function() {
    $ionicLoading.show();
    appService.editProfile($rootScope.formData.profile).then(function(response) {
      if (response.data.success) {
        localStorage.setItem('user', angular.toJson(response.data.user));
        $rootScope.currentUser = response.data.user;
        $.fancybox.close();
      } else {
        $rootScope.formData.profile.errors = response.data.errors;
      }
    }).finally(function(){
      $ionicLoading.hide();
    });
  }

  $scope.editAddress = function() {
    $ionicLoading.show();
    appService.editAddress($rootScope.formData.address).then(function(response) {
      if (response.data.success) {
        localStorage.setItem('user', angular.toJson(response.data.user));
        $rootScope.currentUser = response.data.user;
        $.fancybox.close();
      } else {
        $rootScope.formData.address.errors = response.data.errors;
      }
    }).finally(function(){
      $ionicLoading.hide();
    });
  }

})
.controller('BillingListCtrl', function($scope, $stateParams, billingList) {
  $scope.billingList = billingList.data;
})
.controller('BillingViewCtrl', function($scope, $stateParams, billingView) {
  $scope.billingView = billingView.data;
  $scope.total = function () {
    var total = 0;
    for (i in $scope.billingView.billing_item) {
      total += parseFloat($scope.billingView.billing_item[i].price);
    }
    return total;
  }
})
.controller('ContactCtrl', function($scope, $stateParams, appService) {
  $scope.formData = {subject: 'Demande d’information'};
  $scope.success = false;
  $scope.contact = function() {
    appService.contact($scope.formData).then(function(response){
      if (response.data.success) {
        $scope.success = true;
      } else {
        $scope.formData.errors = response.data.errors;
      }
    });
  }

  if (typeof google != 'undefined') {
    showMap();
  } else {
    $.getScript( "https://maps.googleapis.com/maps/api/js?sensor=true&region=TN", function() {
      showMap();
    });
  }

  function showMap() {
    /**************************************************************/
    /***************** CONFIGURATION GOOGLE MAP ******************/
    /************************************************************/

    var site_name = 'Luxe Decors';
    var latlng = new google.maps.LatLng(35.841886, 10.618829); //Zone d'affichage par defaut
    var zoom = 12;

    var map;

    var image = {
      url: 'assets/img/pin.png',
      size: new google.maps.Size(140, 118),
      origin: new google.maps.Point(0, 0),
      anchor: new google.maps.Point(22, 55),
      scaledSize: new google.maps.Size(70, 59)
    };

    /*******************************************************/
    /************* GOOGLE MAPS API FUNCTIONS **************/
    /*****************************************************/
    //	initializeMap()			resetMap()		deleteOverlays()
    //	showError()				addMarker()		displayDirection()
    //	sortingDistances()		build()
    //	formSubmit				locationClick

    var stylez = [{
      'featureType': 'administrative',
      'elementType': 'all',
      'stylers': [{
        'visibility': 'on'
      }, {
        'saturation': -100
      }, {
        'lightness': 20
      }]
    }, {
      'featureType': 'road',
      'elementType': 'all',
      'stylers': [{
        'visibility': 'on'
      }, {
        'saturation': -100
      }, {
        'lightness': 40
      }]
    }, {
      'featureType': 'water',
      'elementType': 'all',
      'stylers': [{
        'visibility': 'on'
      }, {
        'saturation': -10
      }, {
        'lightness': 30
      }]
    }, {
      'featureType': 'landscape.man_made',
      'elementType': 'all',
      'stylers': [{
        'visibility': 'simplified'
      }, {
        'saturation': -60
      }, {
        'lightness': 10
      }]
    }, {
      'featureType': 'landscape.natural',
      'elementType': 'all',
      'stylers': [{
        'visibility': 'simplified'
      }, {
        'saturation': -60
      }, {
        'lightness': 60
      }]
    }, {
      'featureType': 'poi',
      'elementType': 'all',
      'stylers': [{
        'visibility': 'off'
      }, {
        'saturation': -100
      }, {
        'lightness': 60
      }]
    }, {
      'featureType': 'transit',
      'elementType': 'all',
      'stylers': [{
        'visibility': 'off'
      }, {
        'saturation': -100
      }, {
        'lightness': 60
      }]
    }];

    function initializeMap() {
      var myOptions = {
        zoom: zoom,
        center: latlng,
        // panControl: true,
        streetViewControl: false,
        mapTypeControl: false,
        mapTypeId: google.maps.MapTypeId.ROADMAP
      };
      map = new google.maps.Map(document.getElementById('mapcontact'), myOptions);
      var jayzMapType = new google.maps.StyledMapType(stylez, {
        name: site_name
      });
      map.mapTypes.set(site_name, jayzMapType);
      map.setMapTypeId(site_name);

      var marker = new google.maps.Marker({
        map: map,
        position: latlng,
        icon: image
      });

      var latlng0 = new google.maps.LatLng(35.8421266, 10.6178188);
      var marker0 = new google.maps.Marker({
        map: map,
        position: latlng0,
        icon: image
      });
      var latlng1 = new google.maps.LatLng(34.7482203, 10.7592261);
      var marker1 = new google.maps.Marker({
        map: map,
        position: latlng1,
        icon: image
      });
      var latlng2 = new google.maps.LatLng(35.8354252, 10.6014358);
      var marker2 = new google.maps.Marker({
        map: map,
        position: latlng2,
        icon: image
      });
    }
    initializeMap();
  }

})
.controller('FaqCtrl', function($scope, $stateParams) {
  $("main").fitVids();
  setTimeout(function() {
    $('#faq .faq-question').click(function() {
      $(this).next().slideToggle('normal').parent().toggleClass('active').siblings().removeClass('active').children('.faq-summary').slideUp('normal');
    });
  }, 500);
})
.controller('StoresCtrl', function($scope, $stateParams) {

  if (typeof google != 'undefined') {
    showMap();
  } else {
    $.getScript( "https://maps.googleapis.com/maps/api/js?sensor=true&region=TN", function() {
      showMap();
    });
  }

  function showMap() {
    // var stores= [
  	// 	{"latlng": [45.549283,-73.188579], "address":"510 Montée des Trentes,<br>Mont St-Hilaire, QC J3H 2R8", "name":"La Maison Amérindienne", "phone":"Tel: (450) 464-2500" },
  	// 	{"latlng": [45.539275,-73.509366], "address":"123 St-Charles Ouest,<br>Longueuil, QC J4H 1G7", "name":"William J. Walter", "phone":"Tel: (450) 748-0744" },
  	// 	{"latlng": [45.502036,-73.512777], "address":"436 av Victoria,<br>ST-Lambert, QC J4P 2J4", "name":"Terre Essence", "phone":"T. (450) 812-0222" },
  	// 	{"latlng": [45.529608,-73.344584], "address":"1380 de Montarville,<br>ST-Bruno, QC J3V 3T5", "name":"William J. Walter", "phone":"Tel: (450) 461-3033" },
  	// 	{"latlng": [58.105819,-68.404655], "address":"5205 Airport Road<br> Kuujjuaq, Nunavik, Qc, Canada, J0M 1C0", "name":"TIVI Galleries", "phone":"Tel: 1 (800) 964-2465" },
  	// 	{"latlng": [45.536275,-73.613872], "address":"280 Place du Marché du Nord<br> Montréal, QC H2S 1A1", "name":"Marché Des Saveurs Du Québec", "phone":"Tel: (514) 271-3811" },
  	// 	{"latlng": [45.477875,-73.60139], "address":"330A av. Victoria<br> Westmount QC H3Z 2M8", "name":"L'Autre choix", "phone":"Tel: (514) 369-1888" },
  	// 	{"latlng": [45.336163,-73.267983], "address":"1005 Boul. Du Séminaire Nord<br> St-Jean sur Richelieu,  QC  J3A 1R7", "name":"Patisserie Bissonnette", "phone":"Tel: (514) 349-2375" },
  	// 	{"latlng": [45.624279,-72.94635], "address":"1660, des Cascades<br>Saint-Hyacinthe, QC J2S 3H8", "name":"Les Passions de Manon", "phone":"Tel: (450) 250-6423" },
  	// 	{"latlng": [45.483424,-72.759532], "address":"5, route 137<br>Ste-Cécile de Milton, QC J0E 2C0", "name":"Verger de la Colline", "phone":"Tel: (450) 777-2442" },
  	// 	{"latlng": [58.100782,-68.411098], "address":"Immeuble 601, route de l'Aéroport, C.P. 729,<br>Kuujjuaq (Québec), J0M 1C0", "name":"Nunavik Créations", "phone":"Tel: (819) 964-1848" },
  	// 	{"latlng": [61.358005,-117.659929], "address":"#1 Mackenzie Drive, Fort Providence<br> Northwest Territories, Canada, X0E-0L0", "name":"Snowshoe Inn (NWT) Ltd", "phone":"Tel: (867) 699-3511" },
  	// 	{"latlng": [62.462109,-114.353083], "address":"5005 Bryson Drive<br> Yellowknife, Nt X1A 2A3", "name":"Gallery of the Midnight Sun", "phone":"Tel: (867) 873-8064" },
  	// 	{"latlng": [35.8567859,10.597317], "address":"Immeuble Yasmine Laartiri, Avenue de la république<br>Hammam Sousse, 4011", "name":"Accès Leader", "phone":"Tel: (+216) 73 370 263" }
  	// ];

  	/*______________________________________________________________*/
  	/*_______________________GEOLOCALISATION_______________________*/
  	/*____________________________________________________________*/

  	/**************************************************************/
  	/***************** CONFIGURATION GOOGLE MAP ******************/
  	/************************************************************/

    var site_name="Luxe Decors";

    var noResultsError="<b>Aucun résultat trouvé</b>S'il vous plaît entrer une adresse valide ou ville pour trouver un magasin.";
    var stores=[{"latlng":["34.7482203","10.7592261"],"address":"Route de Teniour Sfax Tunisie","name":"Luxe Decors SFAX-Teniour","phone":"Tel: 22496999"},{"latlng":["35.8421266","10.6178188"],"address":"Avenue Maghreb Arabe Kh\u00e9zama 4051 Sousse \u2013 Tunisie","name":"Luxe Decors SOUSSE- Kh\u00e9zama","phone":"Tel: 73277747-58300858"},{"latlng":["35.8354252","10.6014358"],"address":"Avenue Mongi Grira Sahloul4-4054 Sousse Tunisie","name":"Luxe Decors SOUSSE-Sahloul 4","phone":"Tel: 73822770-58300859"}];
  	var latlng = new google.maps.LatLng(35.4555555, 9.859999); //Zone d'affichage par defaut
  	var zoom = 7;
  	var maxDistance = 100;
  	var defaultStoresList = $('#localisation_result').html();

  	var map;
  	var markersArray = [];
  	var infobox;
  	var bonhomme;
  	var initial = true;

  	var bounds = new google.maps.LatLngBounds();
  	var geocoder = new google.maps.Geocoder();
  	var userAddress ;
  	var directionsDisplay = new google.maps.DirectionsRenderer({suppressMarkers:true, polylineOptions:{strokeColor: '#48812e'}});
  	var directionsService = new google.maps.DirectionsService();

  	var markers = new Array ();
  	for (var i=1; i<=13; i++) {
  		markers[i] = new google.maps.MarkerImage('assets/img/pins.png', new google.maps.Size(32, 47),new google.maps.Point(0,47*(i-1)), new google.maps.Point(16, 47) );
  	}

  	//var marker = new google.maps.MarkerImage(base_url+'assets/img/pin.png', new google.maps.Size(32, 47),new google.maps.Point(0,0), new google.maps.Point(16, 47) );

  	var shadow = new google.maps.MarkerImage('assets/img/shadow.png', new google.maps.Size(40, 24),new google.maps.Point(0,0), new google.maps.Point(9, 21));
  	var pin_user = new google.maps.MarkerImage('assets/img/pin_user.png', new google.maps.Size(27, 31),new google.maps.Point(0,0),new google.maps.Point(14, 25) );
  	var pin_user_shadow = new google.maps.MarkerImage('assets/img/pin_user.png', new google.maps.Size(43, 20),new google.maps.Point(27,0),new google.maps.Point(16, 13) );

  	/*******************************************************/
  	/************* GOOGLE MAPS API FUNCTIONS **************/
  	/*****************************************************/
  	//	initializeMap()			resetMap()		deleteOverlays()
  	//	showError()				addMarker()		displayDirection()
  	//	sortingDistances()		build()
  	//	formSubmit				locationClick

  	function initializeMap () {
  		if (!initial) return;

  		/*var stylez = [ { featureType: "water", stylers: [ { color: "#dcd9c5" } ] },{ featureType: "landscape", stylers: [ { color: "#f7f7f2" } ] },{ featureType: "poi", stylers: [ { color: "#ebe9df" } ] },{ featureType: "road", stylers: [ { color: "#c3be9c" } ] },{ featureType: "road", elementType: "labels.text.fill", stylers: [ { color: "#ffffff" } ] },{ elementType: "labels.text.fill", stylers: [ { color: "#655f3d" } ] } ];*/
  		var stylez = [{"featureType":"administrative","elementType":"all","stylers":[{"visibility":"on"},{"saturation":-100},{"lightness":20}]},{"featureType":"road","elementType":"all","stylers":[{"visibility":"on"},{"saturation":-100},{"lightness":40}]},{"featureType":"water","elementType":"all","stylers":[{"visibility":"on"},{"saturation":-10},{"lightness":30}]},{"featureType":"landscape.man_made","elementType":"all","stylers":[{"visibility":"simplified"},{"saturation":-60},{"lightness":10}]},{"featureType":"landscape.natural","elementType":"all","stylers":[{"visibility":"simplified"},{"saturation":-60},{"lightness":60}]},{"featureType":"poi","elementType":"all","stylers":[{"visibility":"off"},{"saturation":-100},{"lightness":60}]},{"featureType":"transit","elementType":"all","stylers":[{"visibility":"off"},{"saturation":-100},{"lightness":60}]}];

  		var myOptions = {
  			zoom: zoom,
  			center: latlng,
  			// panControl: true,
  			streetViewControl: false,
  			mapTypeControl : false,
  			mapTypeId: google.maps.MapTypeId.ROADMAP,
  			mapTypeControlOptions: {
  				mapTypeIds: [google.maps.MapTypeId.ROADMAP, site_name]
  			}
  		};
  		map = new google.maps.Map(document.getElementById("map"),myOptions);
  		var jayzMapType = new google.maps.StyledMapType(stylez, { name: site_name});
  		map.mapTypes.set(site_name, jayzMapType);
  		map.setMapTypeId(site_name);

  		for (i in stores) {
  			stores[i].latlng = new google.maps.LatLng (stores[i].latlng[0],stores[i].latlng[1]);
  			//addMarker(i, markers[i]);
  		}

  		initial = false;
  	}

  	function resetMap () {
  		deleteOverlays();
  		// for (i in stores)
  		// 	addMarker(i, markers[i]);

  		map.setZoom(zoom);
  		map.setCenter(latlng);
  		$('#localisation_result').html(defaultStoresList);
  	}

  	function deleteOverlays() {
  		if (markersArray) {
  			for (i in markersArray) {
  				markersArray[i].setMap(null);
  			}
  			markersArray.length = 0;
  		}

  		if (bonhomme) bonhomme.setMap(null);
  		if (infobox) infobox.close();
  		directionsDisplay.setMap(null);
  		bounds = new google.maps.LatLngBounds();
  	}

  	function showError (msg) {
  		$('#localisation_result').hide().empty().append('<div id="error">'+msg+'</div>').slideDown();
  		deleteOverlays();
  	}

  	function addMarker(index, icon) {
  		var marker = new google.maps.Marker({
  			map: map,
  			position: stores[index].latlng,
  			icon: icon,
  			shadow:shadow
  		});
  		google.maps.event.addListener(marker, "click", function() {
  			var content = $('<div></div>');
  			content.append('<h2>'+stores[index].name+'</h2><p>'+stores[index].address+'<br>'+stores[index].phone+'</p><span></span>');
  			var myOptions = {
  				content: content.get(0),
  				alignBottom: true,
  				pixelOffset: new google.maps.Size(-121, -62),
  				closeBoxMargin: "-2px -4px 7px 7px",
  				closeBoxURL: "assets/img/close_infobox.png",
  				infoBoxClearance: new google.maps.Size(85, 20)
  			};

  			if (infobox) infobox.close();
  			infobox = new InfoBox(myOptions);
  			infobox.open(map, marker);
  		});

  		markersArray.push(marker);
  	}

  	function displayDirection (start, end) {
  		var request = {
  			origin:start,
  			destination:end,
  			travelMode: google.maps.TravelMode.DRIVING
  		};
  		directionsService.route(request, function(result, status) {
  			if (status == google.maps.DirectionsStatus.OK) {
  				directionsDisplay.setMap(map);
  				directionsDisplay.setDirections(result);
  				directionsDisplay.setOptions({polylineOptions:{strokeColor:'#ec008c'}});
  			}
  		});

  		bonhomme = new google.maps.Marker({
  			map:map,
  			draggable:true,
  			animation: google.maps.Animation.DROP,
  			position: start,
  			icon: pin_user,
  			shadow: pin_user_shadow
  		});
  		google.maps.event.addListener(bonhomme, 'click', function () {
  			 if (bonhomme.getAnimation() != null) {
  				bonhomme.setAnimation(null);
  			  } else {
  				bonhomme.setAnimation(google.maps.Animation.BOUNCE);
  			  }
  		});
  		google.maps.event.addListener(bonhomme, 'dragend', function () {
  			var pos = bonhomme.getPosition();
  			var request = {
  				origin:pos,
  				destination:end,
  				travelMode: google.maps.TravelMode.DRIVING
  			};
  			directionsService.route(request, function(result, status) {
  				if (status == google.maps.DirectionsStatus.OK) {
  				  directionsDisplay.setDirections(result);
  				}
  			});
  		});
  	}

  	function sortingDistances(r){
  		var t = new Array();
  		for( var i= 0 ; i< r.length; i++){
  			r[i].index = i;
  		}
  		for( var i= 0 ; i< r.length; i++){
  			var pmax = -1; var vmax = -1;
  			for ( var j=i; j< r.length; j++){
  			   if( r[j].status==="OK" && r[j].distance.value > vmax) {
  				   pmax = j;
  				   vmax = r[j].distance.value;
  			   }
  			}
  			if ( pmax >-1) {
  				t.push(r[pmax]);
  				var aux = r[i]; r[i] = r[pmax]; r[pmax] = aux;
  			}
  		}
  		return t.reverse();
  	}

  	function build (response, status) {
  		if (status == google.maps.DistanceMatrixStatus.OK) {
  			$('#localisation_result').empty();
  			deleteOverlays();
  			var results = sortingDistances (response.rows[0].elements);
  			if (!results.length) {
  				showError(noResultsError);
  				return;
  			}
  			displayDirection (userAddress, stores[results[0].index].latlng);
  			var k=results.length<3?results.length:3;
  			for (var j=0; j < k; j++) {
  				addMarker(results[j].index, markers[j+1]);
  				var innerhtml='<div class="location" addressIndex="'+results[j].index+'"><span class="marker marker'+(j+1)+'"><span>'+results[j].distance.text+
  					'</span></span><div class="store_info"><h2>'+stores[results[j].index].name+'</h2><p>'+stores[results[j].index].address+
  					'</p>'+stores[results[j].index].phone+'</div></div>';

  				$('#localisation_result').hide().append (innerhtml).slideDown();
  			}
  		} else {
  			showError('<b>Oops! An error occurred in Google Maps API</b>Distance Matrix service was not successful for the following reason: '+ status);
  		}
  	}

  	$('#localization_form').submit(function(e) {
  		var code_postal = $(this).find('input').val();
  		$('#reset').show();
  		if ($.trim(code_postal) == '') {
  			showError(noResultsError);
  			return false;
  		}
  		var service = new google.maps.DistanceMatrixService();
  		geocoder.geocode( { 'address': code_postal}, function(result, status) {
  			if (status == google.maps.GeocoderStatus.OK) {
  				userAddress = result[0].geometry.location;
  				var destinations = new Array ();
  				for (i in stores) {
  					destinations.push(stores[i].latlng);
  				}
  				service.getDistanceMatrix ({
  					origins: [code_postal],
  					destinations: destinations,
  					travelMode: google.maps.TravelMode.DRIVING,
  					unitSystem: google.maps.UnitSystem.METRIC,
  					avoidHighways: false,
  					avoidTolls: false
  				}, build);

  			} else if (status == google.maps.GeocoderStatus.ZERO_RESULTS) {
  				showError(noResultsError);
  			} else {
  				showError('<b>Oops! An error occurred in Google Maps API</b>Geocoding service was not successful for the following reason: '+ status);
  			}
  		});
  		e.preventDefault();
  	});

  	$('#localisation_result').on('click', '.location' , function() {
  		var index = $(this).attr('addressIndex');
  		bounds = new google.maps.LatLngBounds();
  		bounds.extend(stores[index].latlng);
  		map.fitBounds(bounds);
  		map.setZoom(15);
  	});

  	$('#reset').click(function() {
  		$(this).hide().prev('input').val('');
  		resetMap();
  	});

  	initializeMap();
  }
})
.controller('TermsAndConditionsCtrl', function($scope, $stateParams) {

})
.controller('PageCtrl', function($scope, $stateParams) {

})
