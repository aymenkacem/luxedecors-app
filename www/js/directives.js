angular.module('app.directives', [])
    .directive('royalSlider', function () {
        return {
            restrict: 'AEC',
            scope: {
                content: '=',
                rsoptions: '='
            },
            link: function (scope, element, attrs) {
                scope.$watch('content', function () {
                    if (scope.content !== undefined) {
                        setTimeout(function () {
                            var royalSlider = $(element).royalSlider(scope.rsoptions);
                            $(element).find('.rsSlide').addClass("preload");
                          	var slider = $(element).data("royalSlider");
                          	slider.slides[0].holder.on("rsAfterContentSet", function() {
                          	    $(element).find(".rsSlide").removeClass("preload");
                          	});
                        }, 1);
                    }
                });
            }
        }
    })
    .directive('fancybox', function () {
        return {
            restrict: 'A',
            link: function (scope, element, attrs) {
                $(element).fancybox({
                    padding: 0,
                    height: 'auto',
                    autoSize: false,
                    //autoResize: true,
                    //autoCenter: true,
                    scrolling: 'no',
                    //openEffect :  'fade',
                    scrollOutside: false,
                    closeBtn: false,
                    //openMethod : 'zoomIn' ,
                    openSpeed: 500,
                    //closeMethod : 'dropOut',
                    closeSpeed: 300,
                    href: attrs.url,
                    helpers: {
                      overlay: {
                        locked: true
                      }
                    },
                    beforeLoad: function () {
                        var width = parseInt(this.element.data('fancybox-width'));
                        if (width) this.width = width;
                        // this.height = parseInt(this.element.data('fancybox-height'));
                    },
                    afterShow: function () {
                        $('.scroll-content').addClass('disable-scroll');
                        $('.popin-close, .popin-btn-close').click(function () {
                            $.fancybox.close();
                        })
                    },
                    afterClose: function() {
                      $('.scroll-content').removeClass('disable-scroll');
                    }
                });

            }
        }
    });
