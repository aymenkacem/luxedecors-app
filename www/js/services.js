angular.module('app.services', [])

.factory('appService', function($http, SERVER_PATH, $q){
  var deferred = $q.defer();
  var response = {};
  return {
    loadData: function(date) {
        return $http.get(SERVER_PATH + '/loadData');
    },
    getSettings: function() {
      return $http.get(SERVER_PATH + '/getInfo');
    },
    home: function() {
      if (localStorage.getItem('home')) {
        response.data = angular.fromJson(localStorage.getItem('home'))
        deferred.resolve(response);
        return deferred.promise;
      } else {
        return $http.get(SERVER_PATH + '/home');
      }
    },
    newsletter: function(email) {
      return $http({
        method  : 'POST',
        url     : SERVER_PATH + '/newsletter',
        data    : $.param({email: email}),
        headers : { 'Content-Type': 'application/x-www-form-urlencoded' }
      });
    },
    signup: function(formData) {
      return $http({
        method  : 'POST',
        url     : SERVER_PATH + '/signup',
        data    : $.param(formData),
        headers : { 'Content-Type': 'application/x-www-form-urlencoded' }
      });
    },
    contact: function(formData) {
      return $http({
        method  : 'POST',
        url     : SERVER_PATH + '/contact',
        data    : $.param(formData),
        headers : { 'Content-Type': 'application/x-www-form-urlencoded' }
      });
    },
    retrievePass: function(email) {
      return $http({
        method  : 'POST',
        url     : SERVER_PATH + '/retrievePass',
        data    : $.param({email: email}),
        headers : { 'Content-Type': 'application/x-www-form-urlencoded' }
      });
    },
    editProfile: function (data) {
      return $http({
        method  : 'POST',
        url     : SERVER_PATH + '/editProfile',
        data    : $.param(data),
        headers : { 'Content-Type': 'application/x-www-form-urlencoded' }
      });
    },
    editAddress: function (data) {
      return $http({
        method  : 'POST',
        url     : SERVER_PATH + '/editAddress',
        data    : $.param(data),
        headers : { 'Content-Type': 'application/x-www-form-urlencoded' }
      });
    }
  };
})
.factory('productService', function($http, SERVER_PATH, $q){
  var deferred = $q.defer();
  var response = {};
  return {
    getList: function (id, page) {
      var cache = localStorage.getItem('productList_' + id + '_' + page);
      if (cache) {
        response.data = angular.fromJson(cache);
        deferred.resolve(response);
        return deferred.promise;
      } else {
        var url = SERVER_PATH + '/productList/' + id + '/' + page ;
        return $http.get(url);
      }
    },
    search: function (data) {
      return $http({
        method  : 'POST',
        url     : SERVER_PATH + '/search',
        data    : $.param(data),
        headers : { 'Content-Type': 'application/x-www-form-urlencoded' }
      });
    },
    productPromotion: function (data) {
      return $http({
        method  : 'POST',
        url     : SERVER_PATH + '/productPromotion',
        data    : $.param(data),
        headers : { 'Content-Type': 'application/x-www-form-urlencoded' }
      });
    },
    getDetail: function (id) {
      if (localStorage.getItem('product_' + id)) {
        response.data = angular.fromJson(localStorage.getItem('product_' + id))
        deferred.resolve(response);
        return deferred.promise;
      } else {
        return $http.get(SERVER_PATH + '/productDetail/' + id);
      }
    }
  };

})
.factory('paymentService', function($http, SERVER_PATH){
  return {
    cartItems: function(cart) {
      return $http.post(SERVER_PATH + '/cartItems', cart);
    },
    validateCoupon: function(coupon_code) {
      return $http({
        method  : 'POST',
        url     : SERVER_PATH + '/validateCoupon',
        data    : $.param({coupon_code: coupon_code}),
        headers : { 'Content-Type': 'application/x-www-form-urlencoded' }
      });
    },
    processCheckout: function (data) {
      return $http({
        method  : 'POST',
        url     : SERVER_PATH + '/processCheckout',
        data    : $.param(data),
        headers : { 'Content-Type': 'application/x-www-form-urlencoded' }
      });
    },
    confirmCheckout: function(paymentMethod, customerAddress, cart, coupon_code) {
      return $http.post(SERVER_PATH + '/confirmCheckout', {paymentMethod: paymentMethod, customerAddress: customerAddress, cart: cart, coupon_code: coupon_code});
    },
    billingList: function() {
      return $http.get(SERVER_PATH + '/billingList');
    },
    billingView: function(id) {
      return $http.get(SERVER_PATH + '/billingView/' + id);
    },
    dashboard: function() {
      return $http.get(SERVER_PATH + '/dashboard');
    }
  };
});
