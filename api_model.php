<?php
class Api_Model extends Model
{
	function __construct()
	{
		parent::Model();
		$this->load->model('product_model');
		$this->load->helper('date');
	}

	function getBanner(){
		$banners=$this->db->select('b.*')->from('banners AS b')->where(array('b.banner_type'=>1,'b.activate'=>1))->orderby('banner_order','ASC')->get()->result();

		$banners_detail=$this->db->select('bd.*')->from('banners_detail AS bd')->get()->result_leader('banner_id','lang');
		$data = [];
		$banner		="banner_".LANG;
		foreach ($banners as $item) {
			$img =base_url().'uploads/banners/'.$item->$banner;
			$id =(isset($banners_detail[$item->banner_id][LANG]))?base_url().$banners_detail[$item->banner_id][LANG]->banner_link:'#';

			$data[]=array('img'=>$img,'id'=>$id);
		}

		return $data;
	}

	function getCustomerById($id) {
		$this -> db -> from('customer');
		$this -> db -> where('customer_id', $id);
		$this -> db -> limit(1);
		$query = $this -> db -> get();
		if($query -> num_rows() == 1){
			return $query->row();
		} else {
			return false;
		}
	}

	function getSideBox(){
		$banners=$this->db->select('b.*')->from('banners AS b')->where(array('b.banner_type'=>3,'b.activate'=>1))->orderby('banner_order','ASC')->get()->result();

		$banners_detail=$this->db->select('bd.*')->from('banners_detail AS bd')->get()->result_leader('banner_id','lang');
		$data = [];
		$banner		="banner_".LANG;
		foreach ($banners as $item) {
			$img =base_url().'uploads/banners/'.$item->$banner;
			$id =(isset($banners_detail[$item->banner_id][LANG]))?base_url().$banners_detail[$item->banner_id][LANG]->banner_link:'#';

			$data[]=array('img'=>$img,'id'=>$id);
		}

		return $data;
	}

	function getProductFeatured(){

		$categorie_list=$this->db->select('*')->from("categories_config as c")->join("categorie_detail AS cd","cd.categorie_id=c.categorie_id")->where("(SELECT ct.categorie_active FROM (categorie as ct) where ct.categorie_id=c.categorie_id) = ",1)->where("(SELECT COUNT(pc.product_id) FROM (categories_config_product as pc) where pc.categorie_id=c.categorie_id) > ",0)->where("cd.lang",LANG)->orderby('categ_order','ASC')->get()->result();


		$this->db->select('pc.*,p.*,TO_DAYS(p.price_special_from) AS special_from,TO_DAYS(p.price_special_to) AS special_to,(SELECT pd.title from(product_detail as pd) where pd.product_id=pc.product_id AND pd.lang="'.LANG.'") as title,(SELECT pd.url_access from(product_detail as pd) where pd.product_id=pc.product_id AND pd.lang="'.LANG.'") as url_access');
		$this->db->from("categories_config_product AS pc");
		$this->db->join("product AS p","p.product_id=pc.product_id");
		$this->db->where("p.active",1);
		$this->db->orderby('pc.order_prod','ASC');

		$categ_product= $this->db->get()->result_leader('categorie_id','product_id');

		$data = [];
		foreach($categorie_list as $categ){
			$id=$categ->categorie_id;
			$name=$categ->categorie_name;

			$products=array();
			foreach($categ_product[$categ->categorie_id] as $key=>$item){
				$product = $item;
				$product->image=base_url().$this->config->item('upload_product').thumb_name($product->image,'_thumb1');
				$products[]=$product;
			}

			$data[]=array('name'=>$name,'id'=>$id,'products'=>$products);
		}

		return $data;

	}

	function getProductList(){
		$limit=12;
        $page = ($this->uri->segment(4))?$this->uri->segment(4):1;

        $categorie_id=$this->uri->segment(3);
        if(!is_numeric($categorie_id)) {return false;}

        $categorie=$this->product_model->categorie($categorie_id);
        if(!$categorie || !$categorie->categorie_id) {redirect();exit;}


        $product_list= array_values($this->product_model->getTotalProducts($categorie->categorie_id,$page,$limit));
				foreach ($product_list as $key => $product) {
					$product_list[$key]->image = base_url().$this->config->item('upload_product') . thumb_name($product_list[$key]->image,'_thumb1');
				}
        $next_page=$this->product_model->getTotalProducts($categorie->categorie_id,$page+1,$limit);

        $data["date"] =to_days(date("Y-m-d"));
        $data['next_page'] =($next_page)?$page+1:FALSE;
        $data["product_list"] = $product_list;
        $data["url_image"] = base_url().'/uploads/product/';
        $data["categorie_id"] = $categorie_id;
        $data['page'] =$page;

        $parents=$this->product_model->categories_parent($categorie_id);
        $children=$this->product_model->getCategories_list($categorie);

        $categ_parent=$categorie;
        if(!$children && $parents){
            $parent=    end($parents);
            $categ_parent=$this->product_model->getCategorie($parent->categorie_id);
            $children=$this->product_model->getCategories_list($categ_parent);
        }


        $start_prod =($this->uri->segment(5))?(($this->uri->segment(5))+1):1;
        $data["start_prod"] =$start_prod;
        $data["total_product"] =$this->product_model->getTotalProducts($categorie->categorie_id)->num_rows();
        $data["categorie_list"] = $this->product_model->getCategories($categorie->categorie_id);

        $data["categorie"] = $categorie;
        $data["categ_parent"] = $categ_parent;
        $data["parents"] = $parents;
        $data["children"] = $children;
        // $data["breadcrumbs"] = $this->product_model->getBreadcrumbs($parents,$categorie);
        // $data["menu_categ"]  = $this->product_model->getMenu_categ($children,$categorie,$categ_parent);

		return $data;

	}

	public function getCartItems($cart) {
		$this->db->select('p.*,TO_DAYS(p.price_special_from) AS special_from,TO_DAYS(p.price_special_to) AS special_to, (SELECT pd.title from(product_detail as pd) where pd.product_id=p.product_id AND pd.lang="'.SITE_LANG.'") as title');
		$this->db->from("product AS p");
		foreach ($cart as $value) {
			$this->db->orwhere('product_id', $value['id']);
		}
		$this->db->where("p.active",1);
		return $this->db->get()->result();
	}

	public function getRepresentativePromotion($cart) {
		$promotions=array();

		$customer_id=($this->db->getwhere('categories_agent_discount',array('customer_id'=>CUSTOMER_ID))->num_rows())?CUSTOMER_ID:0;

		foreach($cart as $row){
			$row = (object) $row;
			if(!isset($promotions[$row->id])){
				$query_categ=$this->db->join('categorie','categorie.categorie_id=product_categorie.categorie_id')->getwhere('product_categorie',array('product_categorie.product_id'=>$row->id,'categorie.categorie_active'=>1));

				$result=$query_categ->result();
				$promtionList=array();
				foreach($result as $categ){

					$parents=$this->db->select("categ.*,(SELECT categories_agent_discount.discount FROM (categories_agent_discount) WHERE categories_agent_discount.categorie_id=categ.categorie_id AND categories_agent_discount.customer_id=".$customer_id.") AS discount")->from("categorie AS categ,categorie AS node")->where('node.categorie_left BETWEEN categ.categorie_left AND categ.categorie_right')->where("node.categorie_id",$categ->categorie_id)->orderby('categ.categorie_left','ASC')->get()->result();

					$promotParent=null;
					foreach($parents as $row2){
						if(!$promotParent || ($row2->categorie_left > $promotParent->categorie_left && $row2->categorie_left < $promotParent->categorie_right && $row2->discount))
						{
							$promotParent=$row2;
						}
					}
					$promtionList[]=abs($promotParent->discount);
				}
				$promotions[$row->id]=($promtionList)?max($promtionList):0;
			}
		}

		return $promotions;
	}

	public function setCart($cart_items)
	{
		// if cart exist get this cart else create a new cart
		$data = array(
				 'session_id' => '' ,
				 'customer_id' => CUSTOMER_ID ,
				 'status' => 1
			);
		$this->db->insert('cart', $data);
		$cart_id = $this->db->insert_id();

		foreach ($cart_items as $cart_item) {
			$cart_item = (object) $cart_item;
			$data = array(
				 'cart_id' => $cart_id ,
				 'product_id' => $cart_item->id ,
				 'quantity' => $cart_item->quantity,
				 //'option_list' => $item_variation,
				 'item_price' => $cart_item->price,
				 'item_reference' => $cart_item->ref,
				 'item_promotion' => ($cart_item->promotion_value) ? $cart_item->promotion_value : 0,
				 'status' => 1
			);
			$this->db->insert('cart_item', $data);
		}

		return $this->db->getwhere('cart', array('cart_id' => $cart_id))->row();

	}

	function setCustomerAddress($customerAddress, $cart) {
		$customerAddress = (object)$customerAddress;
		$data = array(
			'cart_id' => $cart->cart_id,
			'first_name' => @$customerAddress->first_name,
			'last_name' => @$customerAddress->last_name,
			'company' => @$customerAddress->company,
			'address' => @$customerAddress->address,
			'city' => @$customerAddress->city,
			'postal_code' => @$customerAddress->postal_code,
			'phone' => @$customerAddress->phone,
			'tax_number' => @$customerAddress->tax_number,
			'country' => 'TN',
			'state' => @$customerAddress->state,
			'shipping_different' => @isset($customerAddress->shipping_different) ? $customerAddress->shipping_different : NULL,
			'sh_first_name' => @isset($customerAddress->shipping_different) ? $customerAddress->sh_first_name : $customerAddress->first_name,
			'sh_last_name' => @isset($customerAddress->shipping_different) ? $customerAddress->sh_last_name : $customerAddress->last_name,
			'sh_company' => @isset($customerAddress->shipping_different) ? $customerAddress->sh_company : $customerAddress->company,
			'sh_address' => @isset($customerAddress->shipping_different) ? $customerAddress->sh_address : $customerAddress->address,
			'sh_city' => @isset($customerAddress->shipping_different) ? $customerAddress->sh_city : $customerAddress->city,
			'sh_postal_code' => @isset($customerAddress->shipping_different) ? $customerAddress->sh_postal_code : $customerAddress->postal_code,
			'sh_phone' => @isset($customerAddress->shipping_different) ? $customerAddress->sh_phone : $customerAddress->phone,
			'sh_tax_number' => @isset($customerAddress->shipping_different) ? $customerAddress->sh_tax_number : $customerAddress->tax_number,
			'sh_country' => 'TN',
			'sh_state' => @isset($customerAddress->shipping_different) ? $customerAddress->sh_state : $customerAddress->state,
		);

		$this->db->insert('customer_address', $data);
	}

	function getProduct(){
		$product_id = $this->uri->segment(3);
        if(!$this->db->getwhere('product',array('product_id'=>$product_id,'active'=>1))->num_rows()){
            return false;
        }

        $data=$this->product_model->get($product_id);
				$data['product']->image = base_url().$this->config->item('upload_product') . $data['product']->image;
 				if (isset($data["galerie"])) {
					foreach ($data["galerie"] as $key => $galerie) {
						$data["galerie"][$key]->image = base_url().$this->config->item('upload_product') . $data["galerie"][$key]->image;
					}
				}

				if (isset($data["affiliates_product"])) {
					foreach ($data["affiliates_product"] as $key => $galerie) {
						$data["affiliates_product"][$key]->image = base_url().$this->config->item('upload_product') . thumb_name($data["affiliates_product"][$key]->image, '_thumb1');
					}
				}
				$data['product']->timeleft = timespan(now(),strtotime( $data['product']->price_special_to )+(3600*24));


        $categorie=$this->product_model->getCategorie2(NULL,$product_id);

        if(!$categorie->categorie_id){
           return false;
        }

        $data["random_product"]=$this->product_model->getProductsRandom($product_id,$categorie->categorie_id)->result();
				foreach ($data["random_product"] as $key => $product) {
					$data["random_product"][$key]->image = base_url().$this->config->item('upload_product') . thumb_name($data["random_product"][$key]->image,'_thumb1');
				}

        $parents=$this->product_model->categories_parent($categorie->categorie_id);
        $children=$this->product_model->getCategories_list($categorie);

        $data["categorie"] = $categorie;
        $data["parents"] = $parents;
        $data["children"] = $children;

        $data["date"] =to_days(date("Y-m-d"));
        $data["url_image"] = base_url().'/uploads/product/';

        $data['product_pictogramme']=    $this->db->orderby('item_order','asc')->getwhere('product_pictogramme',array('product_id'=>$product_id))->result_leader('pictogramme_id');
        $data["pictogramme_list"]=$this->product_model->liste_pictogramme();
				foreach ($data["pictogramme_list"] as $key => $picto) {
					$data["pictogramme_list"][$key]->thumb = base_url().$this->config->item('upload_pictogrammes') . $data["pictogramme_list"][$key]->thumb;
				}

        $data['product_attr']=    $this->db->orderby('item_order','asc')->getwhere('product_attribut',array('product_id'=>$product_id))->result_leader('attribut_id');
        $data["attr_list"]=$this->product_model->liste_attr();

				foreach($data['product_attr'] as $key => $item){
					// if(!$data["attr_list"][$key]->cart_added){
						$contentSerialize=@unserialize($item->content);
						if(is_array($contentSerialize)){
							$content=(isset($contentSerialize[SITE_LANG]))?$contentSerialize[SITE_LANG]:implode(', ',$contentSerialize);
						}else
							$content=$item->content;

						$data['product_attr'][$key]->content = $content;
					// }
				}
        //$data["product_sum_qty"]=$this->product_model->getSumCart($product_id);

        //$data["last_link"]=(isset($_SERVER['HTTP_REFERER']))?$_SERVER['HTTP_REFERER']:base_url().'project/product/index/'.$categorie->categorie_id;

        return $data;
	}

	function getMenu(){
		$this->db->select("(COUNT(elparent.categorie_id) - 1) as level, categorie_detail.categorie_name AS name, categ.categorie_id AS id,categorie_detail.categorie_desc,categ.*,(SELECT p.image FROM (product as p) where p.product_id=categ.menu_product) AS image");
		$this->db->select("
			(SELECT categorie_id
		       FROM categorie t2
		       WHERE t2.categorie_left < categ.categorie_left AND t2.categorie_right > categ.categorie_right
		       ORDER BY t2.categorie_right-categ.categorie_right ASC
		       LIMIT 1)
			AS parentid"
		);
		$this->db->from('categorie AS categ,categorie AS elparent');
		$this->db->join("categorie_detail","categ.categorie_id=categorie_detail.categorie_id");
		$this->db->where('categ.categorie_left > elparent.categorie_left');
		$this->db->where('categ.categorie_right < elparent.categorie_right');
		$this->db->where('categorie_detail.lang',SITE_LANG);
		$this->db->where('categ.type',1);
		$this->db->where('categ.categorie_active',1);
		$this->db->where('categ.categorie_menu',1);
		$this->db->where('elparent.type',1);
		$this->db->groupby('categ.categorie_id');
		$this->db->orderby('categ.categorie_left','ASC');

		$rs = $this->db->get()->result_array();
		$new = array();

		$parent=$this->db->getwhere('categorie',array('categorie_id'=>1))->row_array();

		foreach ($rs as $index => $row) {
			$row['img']=($row['image'])?base_url().'/uploads/product/'.thumb_name($row['image'], '_thumb2') : $row['image'];
		     $new[$row['parentid']][] = $row;
		}

		$tree = $this->createTree($new, array($parent));

		return $tree[0]['submenu'];
	}

	function createTree(&$list, $parent){
	    $tree = array();
	    foreach ($parent as $k=>$l){
	        if(isset($list[$l['categorie_id']])){
	            $l['submenu'] = $this->createTree($list, $list[$l['categorie_id']]);
	        }
	        $tree[] = $l;
	    }
	    return $tree;
	}

	function getFooterMenu(){
		$data[]=array('id'=>1,'name'=>'test1','img'=>'');
		$data[]=array('id'=>2,'name'=>'test2','img'=>'');

		return $data;
	}

}
